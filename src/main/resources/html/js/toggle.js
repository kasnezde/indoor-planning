function eventFire(el, etype){
  if (el.fireEvent) {
    el.fireEvent('on' + etype);
  } else {
    var evObj = document.createEvent('Events');
    evObj.initEvent(etype, true, false);
    el.dispatchEvent(evObj);
  }
}


function setVisibility(groupId, visibility) {
    var lo = { "properties" : ["line-opacity"], "values" : { "visible" : 1.0, "none" : pathLineOpacity }};
    var bo = { "properties" : ["fill-opacity"],  "values" : { "visible" : polygonFillOpacity, "none" : 0.0 }};
    var po = { "properties" : ["icon-opacity", "text-opacity"],  "values" : { "visible" : 1.0, "none" : 0.0 }};

    var layers = {"base" : bo, "points" : po, "labels" : po, "path" : lo, "path_points" : po};

    for (var layer in layers) {
        obj = layers[layer];

        for (var j = 0; j < layers[layer].properties.length; j++) {
            var property = obj.properties[j];
            var ratio = obj.values[visibility];

            var layerId = groupId + "-" + layer;

            if (typeof map.getLayer(layerId) != 'undefined') {
                map.setPaintProperty(layerId, property, ratio);
            }
        }
    }
}

function loadToggle() {
    levels = $("body").data("levels");
    var layers = document.getElementById('box-levels-toggle');
    layers.innerHTML = '';

    for (var i = 0; i < levels.length; i++) {
        if ($("#toggle-" + levels[i].groupId).length) { // group id already exists
            continue;
        }

        var link = document.createElement('a');
        link.href = '#';
        if ($("body").data("levels")[i].main == true) {
            link.className = 'active';
        }
        link.id = "toggle-" + levels[i].groupId;
        link.groupId = levels[i].groupId;
        link.z = levels[i].z;
        link.textContent = levels[i].name;

        link.onclick = function(e) {
            e.preventDefault();
            e.stopPropagation();
            var layers = document.getElementById('box-levels-toggle');
            var levelGroupId = this.groupId;

            $("body").data("activeGroup", levelGroupId);


            for (var j = 0; j < levels.length; j++) {
                if (levels[j].groupId == levelGroupId) {  
                    setVisibility(levels[j].groupId, 'visible');
                } else {
                    setVisibility(levels[j].groupId, 'none');
                }
            }

            for (var j = 0; j < layers.childNodes.length; j++) {
                layers.childNodes[j].className = 'none';
            }
            this.className = 'active';
            updateMarker($("#from-input"));
            updateMarker($("#to-input"));
            
        };
        var layers = document.getElementById('box-levels-toggle');
        layers.appendChild(link);
        
    }
}

