mapboxgl.accessToken = ACCESS_TOKEN;

var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/' + mapStyle,
    center: [14.91889, 50.41634], // auto-load for Skoda building
    zoom: 17,
    maxZoom: 20
});

map.addControl(new mapboxgl.Navigation());

function updateLayers(result) {
    for (var layer in result.layers) {
        var layerData = result.layers[layer];

        if (typeof map.getLayer(layerData.id) != 'undefined') {
            map.removeLayer(layerData.id);
        } 
        map.addLayer(layerData);
    }
}

function updateSources(result) {
    for (var source in result.sources) {
        if (typeof map.getSource(source) == 'undefined') { // new source
            $("body").data("sources").push(source);
            map.addSource(source, { type: 'geojson', data: result.sources[source] });
        } else {
            map.getSource(source).setData(result.sources[source]);
        }
    }
}

function showPathInfo() {
    instructions = "";
    arr = $('#path-info').data("instructions");

    for (i = 0; i < arr.length; i++) {
        switch (arr[i].type) {
            case "GO_STRAIGHT":
                instructions += "<div class='instr'>Go <strong>" + arr[i].dist.toFixed(0) + " meters </strong>to the<strong> " + arr[i].side + "</strong></div><hr>";
                break;
            case "TURN_RIGHT":
                instructions += "<div class='instr'>Turn <strong>right</strong></div><hr>";
                break;
            case "TURN_LEFT":
                instructions += "<div class='instr'>Turn <strong>left</strong></div><hr>";
                break;
            case "DOOR":
                instructions += "<div class='instr'>Walk through the <strong>door</strong></div><hr>";
                break;
            case "STAIRS":
                instructions += "<div class='instr'>Use the <strong>stairs</strong> from level " + arr[i].from + " to level " + arr[i].to + "</div><hr>";
                break;
            case "ELEVATOR":
                instructions += "<div class='instr'>Use the <strong>elevator</strong> from level " + arr[i].from + " to level " + arr[i].to + "</div><hr>";
                break;
        }
    }
    $("#modal-inner").html(instructions);
    $("#modal-footer").html("<strong>Total distance:</strong> " + $('#path-info').data("distance").toFixed(0) + " m");
}

function setPath(result) {
    updateSources(result);
    updateLayers(result);

    setVisibility($("body").data("activeGroup"), 'visible');

    // $('#distance').html("<h3>Path distance</h3><div style='margin-left: 3px'>" +  result.variables.distance.toFixed(2) + " m</div>");

    $('#path-info').data("distance", result.variables.distance);
    $('#distance').html("<a href='#' id='info-btn' class='button icon short fill-green info'>Path info</a>");

    bindInfoBtn();
}

function bindInfoBtn() {
    if ($(window).width() >= 500) {
        infoBtn = $('#info-btn');
    } else {
        infoBtn = $('#info-small');
    }

    infoBtn.click(function(e) {
        $('#modal-info').css('display', 'block');
        showPathInfo();
      }
    );
    $("#modal-info-close").click(function(e) {
        $('#modal-info').css('display', 'none');
    });
    $(window).click(function(event) {
        if (event.target == $('#modal-info')) {
            $('#modal-info').css('display', 'none');
        }
    })
}

// function flyTo(feature) {
//   map.flyTo({
//     center: feature.geometry.coordinates,
//     zoom: 21
//   });
// }

function getPlaceLabelAsString(feature) {
    var levelId = feature.properties.levelId;
    var buildingId = getLevelById(levelId).buildingId;
    var buildingName = $("body").data("buildingIds")[buildingId];
    var title = feature.properties.title;

    var desc = buildingName + "-" + title;
    return desc;
}

function getCoordinatesByLabel(label) {
    var sources = $("body").data("sources");
    var tokens = label.split(/[\s,-]+/);
    var place = tokens[tokens.length - 1];

    var labelSources = $.grep(sources, function(id,i) {
      return id.endsWith("labels");
    });

    for (var i = 0; i < labelSources.length; i++) {
        var features = map.getSource(labelSources[i])._data.features;

        for (var j = 0; j < features.length; j++) {
            var feature = features[j];
            var levelId = feature.properties.levelId;
            var buildingId = getLevelById(levelId).buildingId;
            var buildingName = $("body").data("buildingIds")[buildingId];

            if (feature.properties.title.toUpperCase() == place.toUpperCase() && buildingName.toUpperCase() == tokens[0].toUpperCase()) {
                point = {
                 "lng": feature.geometry.coordinates[0],
                 "lat": feature.geometry.coordinates[1],
                 "levelGroupId": getLevelGroup(feature.properties.levelId)
                }
                return point;
            } 
        }
    }
    return null;
}

function getLocationFromInputBox(el) {
    var loc;

    if (el.data("lng") != undefined) {
        loc = {
         "lng": el.data("lng"),
         "lat": el.data("lat"),
         "levelGroupId": el.data("levelGroupId")
        }
    } else {
        loc = getCoordinatesByLabel(el.val());
    }
    return loc;
}

function updateMarker(el) {
    var marker = $("#" + el.attr('id') + '-marker')[0];

    if (marker && marker.length != 0) {
        var url = $("body").data("activeGroup") == marker.levelGroupId ? el.data("pin") : el.data("pin_inv");
        marker.style.backgroundImage = 'url(' + url + ')';
    }
}

function addMarkerAtPos(lng, lat, el, levelGroupId) {
    var url = $("body").data("activeGroup") == levelGroupId ? el.data("pin") : el.data("pin_inv");

    point = {
            "type": "Feature",
            "properties": {
                "iconSize": [32, 43]
            },
            "geometry": {
                "type": "Point",
                "coordinates": [lng,lat]
            }
        };

    if ($("#" + el.attr('id') + '-marker').length != 0) {
        $("#" + el.attr('id') + '-marker').remove();
    }

    var marker = document.createElement('div');

    marker.id = el.attr('id') + '-marker';
    marker.className = 'marker';
    marker.style.backgroundImage = 'url(' + url + ')';
    marker.style.width = point.properties.iconSize[0] + 'px';
    marker.style.height = point.properties.iconSize[1] + 'px';
    marker.levelGroupId = levelGroupId;

    new mapboxgl.Marker(marker, {offset: [-point.properties.iconSize[0] / 2 + 2, -point.properties.iconSize[1] / 2 - 20]})
        .setLngLat(point.geometry.coordinates)
        .addTo(map);
}

function addPathMarker(lng, lat, levelGroupId, el) {
    $.ajax({url: APP_SERVER + "/v1/location/nearest/?lat=" + lat + "&lon=" 
            + lng + "&levelGroupId=" +  $("body").data("activeGroup"),
            success: function(result){
                    addMarkerAtPos(result.lon, result.lat, el, levelGroupId);
                    bindNextCursor();
                },
            error: function(xhr){
                    addMarkerAtPos(lng, lat, el, levelGroupId);
                    bindNextCursor();

                    if ($(window).width() >= 500) {
                       $('#path-info').html("<h3>Warning</h3>" + el.data("text") +" is not inside the building.");
                    } else {
                        // alert("Warning: " + el.data("text") + " is not inside the building");
                    }
                },
            });
}

function bindNextCursor() {
    if ($("body").data("lastCursor") == 'from-input') {
        bindCursorForPathSearch($("#to-input"));
    } else {
        bindCursorForPathSearch($("#from-input"));
    }
}

function bindCursorForPathSearch(el) {
    $('#map canvas').css('cursor', 'crosshair');

    $("body").data("lastCursor", el.attr('id'));

    map.off('click');
    map.on('click', function (e) {
        el.val(el.data("text"));
        el.data("lng", e.lngLat.lng);
        el.data("lat", e.lngLat.lat);
        el.data("levelGroupId", $("body").data("activeGroup"));
        el.css('font-style','italic');
        map.off('click');
        $('#map canvas').css('cursor', '');
        addPathMarker(e.lngLat.lng, e.lngLat.lat, $("body").data("activeGroup"), el);
        planFromInputBoxes();
    });
}

function initInputField(el, places) {
    var autocompleteOptions = {
        delay: 150,
        minLength: 0,
        source: function(request, response) {
            var results = $.ui.autocomplete.filter(places, request.term);
            response(results.slice(0, 12));
        }
    }
    el.autocomplete(autocompleteOptions);
    el.focus(function() {
        if ($(this).data("lng") != undefined) {
            $(this).val("");
            $(this).removeData("lng");
            $(this).removeData("lat");
        }

        $(this).css('font-style','normal');

        bindCursorForPathSearch($(this));
        $(this).data("uiAutocomplete").search($(this).val());
      }
    );
    // el.focusout(function() {
    //     $('#map canvas').css('cursor', '');
    // })
    $('.ui-autocomplete').on('click', '.ui-menu-item', function(){
        planFromInputBoxes();
    });
}

function createPanel(data) {
    $("#locations").addClass('hidden');
    $("#places").removeClass('hidden');

    $('#places-back-btn').click(function() {
        $("#places").addClass('hidden');
        $("#locations").removeClass('hidden');
    });

    var places = [];

    for (i = 0; i < data.objectList.length; i++) {
        var level = data.objectList[i];

        for (j = 0; j < level.features.length; j++) {
            var feature = level.features[j];
            var desc = getPlaceLabelAsString(feature);
            places.push(desc);
        }
    }

    $("#from-input").data("pin", "img/markers/marker_a.png");
    $("#from-input").data("pin_inv", "img/markers/marker_a_inv.png");
    $("#from-input").data("text", "Location A");

    $("#to-input").data("pin", "img/markers/marker_b.png");
    $("#to-input").data("pin_inv", "img/markers/marker_b_inv.png");
    $("#to-input").data("text", "Location B");

    initInputField($("#from-input"), places);
    initInputField($("#to-input"), places);

    bindCursorForPathSearch($("#from-input"));

    $('#handicap-toggle').click(function(e) {
        planFromInputBoxes();
      }
    );
}

function planFromInputBoxes() {
    if ($("#from-input").val() == "" || $("#to-input").val() == "") {
        return
    }
    $("#info-small").removeClass('hidden');
    $("#box-info").removeClass('hidden');
    $('#distance').html("");
    $('#path-info').html("<h3>Planning...</h3>");

    start = getLocationFromInputBox($("#from-input"));
    end = getLocationFromInputBox($("#to-input"));

    if (start && end) {
        planPath(start, end);
        addPathMarker(start.lng, start.lat, start.levelGroupId, $("#from-input"));
        addPathMarker(end.lng, end.lat, end.levelGroupId, $("#to-input"));
    }
}

function getLevelById(levelId) { //TODO make more efficient
    levels = $("body").data("levels");

    for (var j = 0; j < levels.length; j++) {
        if (levels[j].id == levelId) {
            return levels[j];
        }
    }
}

function getLevelGroup(levelId) {
    level = getLevelById(levelId);
    return level.groupId;
}

function addPlacesList(id) {
    $.when($.ajax({url: APP_SERVER + "/v1/building/places?id=" + id, 
                    success: function(result) {
                        createPanel(result);
                    },
                    error: function(xhr){
                            $("#info").html("An error occured: " + xhr.status + " " + xhr.statusText);
                        },
                    }));
}

function setMap(result) {
    updateSources(result);
    updateLayers(result);

    map.setCenter([result.variables.startLongitude, result.variables.startLatitude]);
    map.setCenter([result.variables.startLongitude, result.variables.startLatitude]); // bug in mapbox's setCenter()
    map.setZoom(result.variables.zoom);
    map.setZoom(result.variables.zoom);


    $("body").data("buildingIds", result.variables.buildingIds);
    $("body").data("levels", result.variables.levels);
}

// function loadBuildings() {
//     $.when($.ajax({url: APP_SERVER + "/v1/location/list", 
//                     success: function(result) {
//                         createLocationList(result);
//                     },
//                     error: function(xhr){
//                             $("#info").html("An error occured: " + xhr.status + " " + xhr.statusText);
//                         },
//                     }));
// }

function loadMap(id) {
    $.when($.ajax({url: APP_SERVER + "/v1/location/?id=" + id, 
                    success: function(result) {
                        setMap(result);
                    },
                    error: function(xhr){
                            $("#info").html("An error occured: " + xhr.status + " " + xhr.statusText);
                        },
                    })).then(function() {
                        addPlacesList(id);
                        levels = $("body").data("levels");

                        for (var j = 0; j < levels.length; j++) {
                            if (levels[j].main == true) {
                                $("body").data("activeGroup", levels[j].groupId);
                                setVisibility(levels[j].groupId, 'visible');
                                break;
                            }
                        }
                        loadToggle();
                    })
}

function getNearestLabel(e) {
    var activeGroup = $("body").data("activeGroup")
    var nearestLabel = turf.nearest([e.lngLat.lng, e.lngLat.lat], map.getSource(activeGroup + "-labels")._data);

    return nearestLabel;
}

function clearPath() {
    var sources = $("body").data("sources");

    var pathSources = $.grep(sources, function(id,i) {
      return id.endsWith("path");
    });

    for (i=0; i<pathSources.length; i++) {
            map.removeLayer(pathSources[i]);
        map.removeSource(pathSources[i]);

        var index = $("body").data("sources").indexOf(pathSources[i]);
        $("body").data("sources").splice(index, 1);
    }
}

function planPath(start, end) {
      $.ajax({url: APP_SERVER + "/v1/planner/?startLat=" + start.lat + "&startLon=" 
            + start.lng + "&startLevelGroupId=" + start.levelGroupId 
            + "&endLat=" + end.lat + "&endLon=" + end.lng + "&endLevelGroupId=" + end.levelGroupId
            + "&accessibility=" + $("#handicap-toggle")[0].checked,
            success: function(result){
                    setPath(result);
                    $('#path-info').data("instructions", result.variables.instructions);
                    $('#path-info').html("");
                },
            error: function(xhr){
                if ($(window).width() >= 500) {
                   $('#path-info').html("<h3>Path not found</h3>No suitable path between points A and B.");
                } else {
                    // alert("Path not found (no suitable path between points A and B)");
                }
                clearPath();
                $("#info").html("An error occured: " + xhr.status + " " + xhr.statusText);
                },
            });
}

// function createLocationList(data) {
//   for (i = 0; i < data.objectList.length; i++) {
//     var location = data.objectList[i];
//     var listing = $("<div></div>")
//                     .appendTo("#locationlisting")
//                     .addClass("item")
//                     .attr("id","listing-" + i);

//     var link = $("<a></a>")
//         .appendTo(listing)
//         .attr("href", "#")
//         .attr("location-id",location.id)
//         .attr("location-name",location.properties.name)
//         .addClass("title")
//         .text(location.properties.name);

//     $("<div></div>")
//            .appendTo(listing)
//            .text(location.properties.description);

//     link.click(function(e) {
//       loadMap($(e.target).attr('location-id'));

//       var activeItem = $(".active").eq(0);
//       if (activeItem) {
//         activeItem.removeClass("active");
//       }
//     });
//   }
// }


if ($(window).width() <= 500) {
    map["doubleClickZoom"].disable();
}

$("body").data("sources", []);

map.on('load', function() {
    // loadBuildings();
    loadMap('skoda-mb'); // auto-load of Skoda building
});
