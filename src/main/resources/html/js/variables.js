// Mapbox access token
var ACCESS_TOKEN = "pk.eyJ1Ijoia2FzbmV6ZGUiLCJhIjoiY2lzZzN0bXMyMDA0bDJwbnF1d3o0cno1OSJ9.wYotm4p930IHqUht229Acw";

// Server URL
var APP_SERVER = "http://localhost:8080"

// Mapbox map style
var mapStyle = "bright-v9";

// Opacity of path at inactive levels
var pathLineOpacity = 0.2;

// Opacity of building polygons
var polygonFillOpacity = 0.15;

