import processing
import itertools

ptLayer = iface.activeLayer()

lnLayer = QgsMapLayerRegistry.instance().mapLayersByName("tmp_lines")[0]
lnLayer.startEditing()

sel = ptLayer.selectedFeatures()

print "Joining", len(sel), "points"

for sel_pair in itertools.combinations(sel, 2):
    pts = [pt.geometry().asPoint() for pt in sel_pair]
    
    uid_idx = ptLayer.fieldNameIndex('uniqueId')
    uid1 = sel_pair[0].attributes()[uid_idx]
    uid2 = sel_pair[1].attributes()[uid_idx]

    lvl_idx = ptLayer.fieldNameIndex('level')
    lvl = sel[0].attributes()[lvl_idx]

    line1 = QgsFeature()
    line1.setGeometry(QgsGeometry.fromPolyline(pts))
    line2 = QgsFeature()
    line2.setGeometry(QgsGeometry.fromPolyline(list(reversed(pts))))

    from_idx = lnLayer.fieldNameIndex('from')
    to_idx = lnLayer.fieldNameIndex('to')
    line_lvl_idx = lnLayer.fieldNameIndex('level')

    line1.setAttributes([lvl, None, None, uid1, uid2])
    line2.setAttributes([lvl, None, None, uid2, uid1])

    lnLayer.addFeature(line1)
    lnLayer.addFeature(line2)

lnLayer.commitChanges()
lnLayer.updateExtents()

lnLayer.startEditing()
print "OK"
