import processing
from qgis.gui import QgsMapTool
import qgis

class PointTool(QgsMapTool):   
    def __init__(self, canvas):
        QgsMapTool.__init__(self, canvas)
        self.canvas = canvas    

    def canvasPressEvent(self, event):
        pass

    def canvasMoveEvent(self, event):
        x = event.pos().x()
        y = event.pos().y()

        point = self.canvas.getCoordinateTransform().toMapCoordinates(x, y)

    def canvasReleaseEvent(self, event):
        #Get the click
        x = event.pos().x()
        y = event.pos().y()
    

        point = self.canvas.getCoordinateTransform().toMapCoordinates(x, y)
        crsDest = QgsCoordinateReferenceSystem(4326)    # WGS 84
        crsSrc = QgsCoordinateReferenceSystem(3857)  # Mercator
        xform = QgsCoordinateTransform(crsSrc, crsDest)
        point = xform.transform(point)

        print point
        ptLayer = iface.activeLayer()
        
        uid_idx = ptLayer.fieldNameIndex('uniqueId')

        idxs = [feat.attributes()[uid_idx] for feat in ptLayer.getFeatures()]

        idxs_num = [int(idx.split('|')[-1]) for idx in idxs]
        point_id = "|".join(idxs[0].split('|')[:-1] + [str(max(idxs_num)+1)])
        
        level = idxs[0].split('|')[1]
        
        ptFeat = QgsFeature()
        ptFeat.setGeometry(QgsGeometry.fromPoint(point))
        
        ptFeat.setAttributes([level, "inside", point_id, None, None])
        
        print "Adding point id " + point_id
        ptLayer = iface.activeLayer()
        
        ptLayer.startEditing()
        ptLayer.addFeature(ptFeat)
        
        ptLayer.commitChanges()
        ptLayer.updateExtents()

        ptLayer.startEditing()

    def activate(self):
        pass

    def deactivate(self):
        pass

    def isZoomTool(self):
        return False

    def isTransient(self):
        return False

    def isEditTool(self):
        return True
        
tool = PointTool(iface.mapCanvas())
iface.mapCanvas().setMapTool(tool)