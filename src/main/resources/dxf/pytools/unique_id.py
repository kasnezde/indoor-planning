import processing

ptLayer = iface.activeLayer()

uid_idx = ptLayer.fieldNameIndex('uniqueId')

idxs = [feat.attributes()[uid_idx] for feat in ptLayer.getFeatures()]

idxs_num = [int(idx.split('|')[-1]) for idx in idxs]
print "|".join(idxs[0].split('|')[:-1] + [str(max(idxs_num)+1)])