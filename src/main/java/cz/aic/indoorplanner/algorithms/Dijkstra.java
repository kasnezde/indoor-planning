package cz.aic.indoorplanner.algorithms;

import cz.agents.basestructures.Graph;
import cz.aic.indoorplanner.structures.navigation.NavigationEdge;
import cz.aic.indoorplanner.structures.navigation.NavigationNode;
import cz.aic.indoorplanner.structures.navigation.NavigationPath;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

/**
 * A Dijkstra shortest path algorithm.
 */
public class Dijkstra {
    private Graph<NavigationNode, NavigationEdge> graph;
    private Map<NavigationNode, Double> distances;
    private Map<NavigationNode, NavigationNode> predecessors;
    private PriorityQueue<NavigationNode> open;
    private NavigationNode startNode;
    private NavigationNode targetNode;

    private void relax(NavigationNode fromNode, NavigationNode toNode, double dist) {
        double newDistance = distances.get(fromNode) + dist;

        if (distances.get(toNode) > newDistance) {
            distances.put(toNode, newDistance);
            predecessors.put(toNode, fromNode);

            open.remove(toNode);    // PQ does not update automatically
            open.add(toNode);
        }
    }

    private NavigationPath reconstructPath() {
        NavigationPath path = new NavigationPath();
        NavigationNode node = targetNode;

        if (predecessors.get(targetNode) == null) {
            return null;
        }

        while (! node.equals(startNode)) {
            path.addNode(node);
            List<NavigationEdge> edges = graph.getInEdges(node);
            node = predecessors.get(node);

            for (NavigationEdge edge : edges) {
                if (edge.getFromId() == node.getId()) {
                    path.addEdge(edge);
                    break;
                }
            }
        }
        path.addNode(startNode);
        return path;
    }

    public NavigationPath findPath(Graph<NavigationNode, NavigationEdge> graph, NavigationNode startNode, NavigationNode targetNode,
                                   boolean accessibility) {
        this.startNode = startNode;
        this.targetNode = targetNode;
        this.graph = graph;
        distances = new HashMap<>();
        predecessors = new HashMap<>();
        open = new PriorityQueue<>((o1, o2) -> Double.compare(distances.get(o1), distances.get(o2)));

        for (NavigationNode node : graph.getAllNodes()) {
            distances.put(node, Double.MAX_VALUE);
        }
        distances.put(startNode, 0.);
        open.addAll(graph.getAllNodes());

        while (! open.isEmpty()) {
            NavigationNode current = open.remove();

            if (targetNode != null && current.equals(targetNode)) {
                return reconstructPath();
            }

            for (NavigationEdge edge : graph.getOutEdges(current)) {
                double dist = edge.getPreciseLength();
                NavigationNode neighbour = graph.getNode(edge.getToId());

                // cannot use stairs in accessibility mode
                if (accessibility && edge.getObject() != null && edge.getObject().equals("stairs")) {
                    continue;
                }

                // minimize the number of doors used in a plan
                if (neighbour.getProperty("placement").equals("door")) {
                    dist *= 10;
                }
                relax(current, neighbour, dist);
            }
        }
        return null;
    }
}
