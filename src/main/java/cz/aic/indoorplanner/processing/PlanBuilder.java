package cz.aic.indoorplanner.processing;

import com.fasterxml.jackson.core.type.TypeReference;
import cz.aic.indoorplanner.structures.map.Building;
import cz.aic.indoorplanner.structures.info.MapInfo;
import cz.aic.indoorplanner.utils.ConfigUtils;
import org.apache.log4j.Logger;
import cz.aic.indoorplanner.structures.geojson.*;
import cz.aic.indoorplanner.structures.geojson.Point;
import cz.aic.indoorplanner.structures.geometry.Coord;
import cz.aic.indoorplanner.utils.IOUtils;
import cz.aic.indoorplanner.utils.JSONUtils;

import java.util.*;

import static cz.agents.geotools.DistanceUtil.computeGreatCircleDistance;
import static cz.aic.indoorplanner.IndoorPlanner.loadMapInfo;
import static cz.aic.indoorplanner.utils.GeometryUtils.pointsGPSDistance;

/**
 * Creates a navigation graph from a building plan.
 */
public class PlanBuilder {
    private final static Logger logger = Logger.getLogger(PlanBuilder.class);
    private ConfigUtils p = ConfigUtils.getInstance();

    private Map<String, List<Feature>> features;    /** a map linking a level id to a list of geojson features */
    private Map<String, List<Feature>> nodes;       /** a map linking a level id to a list of navigation nodes */
    private Map<String, List<Feature>> edges;       /** a map linking a level id to a list of navigation edges */
    private Map<String, MapInfo.Level> levelInfos;  /** a map linking a level id to an info structure */

    private Building building;                      /** a structure for manipulating with building plan */
    private MapInfo mapInfo;                        /** a map info structure */
    private int pointId;                            /** an id incremented for each node added to the navigation graph */
    private String dir;                             /** a directory to process */

    // constants
    private final double VISIBILITY_THRESHOLD = p.getPropertyAsDouble("visibility_threshold");
    private final double POLYGON_TOUCH_THRESHOLD = p.getPropertyAsDouble("polygon_touch_threshold");
    private final double CLOSEST_DOORS_THRESHOLD = p.getPropertyAsDouble("closest_doors_threshold");
    private final double NEAR_WALL_THRESHOLD = p.getPropertyAsDouble("near_wall_threshold");

    private final double JOIN_NODES_UB = p.getPropertyAsDouble("join_nodes_ub");
    private final double JOIN_NODES_LB = p.getPropertyAsDouble("join_nodes_lb");
    private final double JOIN_DOORS_UB = p.getPropertyAsDouble("join_doors_ub");
    private final double JOIN_DOORS_LB = p.getPropertyAsDouble("join_doors_lb");

    public PlanBuilder() {
        building = new Building();
        features = new HashMap<>();
        nodes = new HashMap<>();
        edges = new HashMap<>();
        levelInfos = new HashMap<>();
    }

    /**
     * Processes a directory {@code dir}. Loads info about available levels from a file {@code dir}/map-info.json,
     * loads those levels and creates files with a navigation graph.
     *
     * @param dir a directory to create a navigation graph in
     */
    public void createGraphFromPlan(String dir) {
        this.dir = dir;
        this.pointId = 0;

        mapInfo = loadMapInfo(dir);

        try {
            for (MapInfo.Level level : mapInfo.getDrawing().getLevels()) {
                String levelId = level.getId();
                levelInfos.put(levelId, level);
                features.put(levelId, loadGeojson(dir + "/" + levelId + ".json"));
            }
        }  catch (NullPointerException e) {
            logger.error("Cannot load GeoJSON.");
            e.printStackTrace();
            return;
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        for (String levelID : features.keySet()) {
            extractWalls(levelID);
            extractDoors(levelID);
            generateNodes(levelID);
            createEdges(levelID);
        }
        createFiles();
    }

    /**
     * Creates edges for a navigation graph. Constants determine which nodes will be joined (crossing walls,
     * joining nodes through doors, etc.)
     *
     * @param levelId an id of the level to be processed
     */
    private void createEdges(String levelId) {
        edges.computeIfAbsent(levelId, v -> new ArrayList<>())
                .addAll(joinVisiblePoints(nodes.get(levelId), 0, JOIN_NODES_LB, JOIN_NODES_UB));
        edges.computeIfAbsent(levelId, v -> new ArrayList<>())
                .addAll(joinVisiblePoints(nodes.get(levelId), building.getDoors(levelId), 0, VISIBILITY_THRESHOLD, JOIN_DOORS_LB, JOIN_DOORS_UB));
        edges.computeIfAbsent(levelId, v -> new ArrayList<>())
                .addAll(joinVisiblePoints(building.getDoors(levelId), nodes.get(levelId), VISIBILITY_THRESHOLD, 0, JOIN_DOORS_LB, JOIN_DOORS_UB));
        edges.computeIfAbsent(levelId, v -> new ArrayList<>())
                .addAll(joinVisiblePoints(building.getDoors(levelId), VISIBILITY_THRESHOLD, JOIN_DOORS_LB, JOIN_DOORS_UB));

        System.out.println("Level " + levelId + ": Created " + edges.get(levelId).size() + " edges");
    }

    /**
     * Creates files with a navigation graph in the directory.
     */
    private void createFiles() {
        for (String levelId : features.keySet()) {
            List<Feature> navigationFeatures = new ArrayList<>();
            navigationFeatures.addAll(building.getDoors(levelId));

            navigationFeatures.addAll(nodes.get(levelId));
            navigationFeatures.addAll(edges.get(levelId));
            writeGeojson(dir + "/" + levelId + ".nav.json", navigationFeatures);
        }

    }

    /**
     * Loads a geojson file.
     *
     * @param filename a name of the file to be loaded
     * @return a list of features contained in the geojson file
     */
    private List<Feature> loadGeojson(String filename) {
        String jsonStr = IOUtils.readTextFile(filename);
        FeatureCollection featureCollection = JSONUtils.convertJSONStringToDesiredObject(jsonStr,
                new TypeReference<FeatureCollection>() {});
        return featureCollection.getFeatures();
    }

    /**
     * Creates a geojson file.
     *
     * @param filename a name of the file to be created
     * @param features a list of geojson features
     */
    private void writeGeojson(String filename, List<Feature> features) {
        FeatureCollection navigationJSON = new FeatureCollection(features);
        String json = JSONUtils.javaObjectToJSON(navigationJSON);
        IOUtils.writeTextFile(filename, json);
    }

    /**
     * Finds walls in the building plan and saves them to the Building object for later use.
     *
     * @param levelId an id of the level to be processed
     */
    private void extractWalls(String levelId) {
        String wallLayerName = p.getProperty("wall_layer_name");

        for (Feature feature : features.get(levelId)) {
            if (feature.getProperty("Layer").equals(wallLayerName)) {
                building.addWall(levelId, feature);
            }
        }
    }

    /**
     * Finds doors in the building plan and saves them to the Building object for later use.
     *
     * @param levelId an id of the level to be processed
     */
    private void extractDoors(String levelId) {
        String doorSubclass = p.getProperty("door_subclass");

        for (Feature feature : features.get(levelId)) {
            String subClasses = feature.getProperty("SubClasses");

            if (subClasses.contains(doorSubclass)) {
                Coord doorCoords = getDoorCoords(levelId, feature);

                if (doorCoords != null && building.getNearestDoorDistance(levelId, doorCoords) > CLOSEST_DOORS_THRESHOLD) {
                    Feature point = new Feature(new Point(doorCoords));
                    point.setId(pointId++);
                    point.addProperty("level", levelId);
                    point.addProperty("placement", "door");
                    point.addProperty("uniqueId", mapInfo.getId() + "|" + levelId + "|" + pointId);

                    building.addDoor(levelId,point);
                }
            }
        }
    }

    /**
     * Tries to estimate a position of the doors. Looks for a point where an arc (supposedly a door
     * or its part) touches a wall.
     *
     * @param levelId an id of the level where the doors should be located
     * @param feature an arc represented by a geojson polyline
     * @return a point where the arc touches a wall, null if the nearest point distance is above threshold
     */
    private Coord getDoorCoords(String levelId, Feature feature) {
        Polyline line = (Polyline) feature.getGeometry();

        Coord tangentPoint = building.getPointCloseToAWall(levelId, line, POLYGON_TOUCH_THRESHOLD);

        return tangentPoint;
    }

    /**
     * Generates a node grid. The grid represents accessible places on the map.
     *
     * The grid is generated in the following manner: data about the top left corner [TL], top right corner [TR]
     * and bottom left corner [BL] of the level (all manually recorded in the level info file) are used to find
     * two main axes - TL-TR and TL-BL. Then an equidistant grid of given density is generated parallel
     * to those two axes (excluding nodes which would be too close to a wall). Borders of the grid are set according to
     * the parallelogram given by the corners. This follows the geometry of the building and generates "good-looking" paths.
     *
     * @param levelId an id of the level to be processed
     */
    private void generateNodes(String levelId) {
        MapInfo.GridAxes gridAxes = levelInfos.get(levelId).getGrid_axes();

        // get coordinates of the corners
        double TOP_LEFT_X = gridAxes.getTop_left_x();
        double TOP_LEFT_Y = gridAxes.getTop_left_y();
        double TOP_RIGHT_X = gridAxes.getTop_right_x();
        double TOP_RIGHT_Y = gridAxes.getTop_right_y();
        double BOTTOM_LEFT_X = gridAxes.getBottom_left_x();
        double BOTTOM_LEFT_Y = gridAxes.getBottom_left_y();

        int DENSITY_AXIS_1 = (int) computeGreatCircleDistance(TOP_LEFT_X, TOP_LEFT_Y,
                TOP_RIGHT_X, TOP_RIGHT_Y);

        int DENSITY_AXIS_2 = (int) computeGreatCircleDistance(TOP_LEFT_X, TOP_LEFT_Y,
                BOTTOM_LEFT_X, BOTTOM_LEFT_Y);


        for (int i = 0; i < DENSITY_AXIS_1; i++) {
            double alpha1 = (1.0 / DENSITY_AXIS_1) * i;
            double xAxis1 = alpha1 * TOP_LEFT_X + (1-alpha1) * TOP_RIGHT_X;
            double yAxis1 = alpha1 * TOP_LEFT_Y + (1-alpha1) * TOP_RIGHT_Y;

            for (int j = 0; j < DENSITY_AXIS_2; j++) {
                double alpha2 = (1.0 / DENSITY_AXIS_2) * j;
                double xDir = BOTTOM_LEFT_X - TOP_LEFT_X;
                double yDir = BOTTOM_LEFT_Y - TOP_LEFT_Y;

                Coord ptCoord = new Coord(yAxis1 + alpha2 * yDir, xAxis1 + alpha2 * xDir);

                if (building.contains(levelId, ptCoord) && building.getNearestWallDistance(levelId, ptCoord) > NEAR_WALL_THRESHOLD) {
                    Feature point = new Feature(new Point(ptCoord));
                    point.setId(pointId++);
                    point.addProperty("level", levelId);
                    point.addProperty("placement", "inside");
                    point.addProperty("uniqueId", mapInfo.getId() + "|" + levelId + "|" + pointId);

                    nodes.computeIfAbsent(levelId, v -> new ArrayList<>()).add(point);
                }
            }
        }
    }

    /**
     * A wrapper for {@code joinVisiblePoints} when thresholds are equal
     */
    private List<Feature> joinVisiblePoints(List<Feature> points, double threshold, double minDistance, double maxDistance) {
        return joinVisiblePoints(points, points, threshold, threshold, minDistance, maxDistance);
    }

    /**
     * Joins all points between sets which are visible, taking into account the thresholds.
     *
     * Constants thresholdA and thresholdB can be used for doors which usually touch a wall,
     * but still need to be visible by the other points.
     *
     * @param pointsA a first set of points
     * @param pointsB a second set of points
     * @param thresholdA a distance in which a wall can appear near a point from pointsA
     *                   and the point is still considered as visible
     * @param thresholdB a distance in which a wall can appear near a point from pointsB
     *                   and the point is still considered as visible
     * @param minDistance a minimal distance between two points which will be joined
     *                    (setting it too low can cause unnecessary joining of accidental bundles of points)
     * @param maxDistance a maximal distance between two points which will be joined
     * @return
     */
    private List<Feature> joinVisiblePoints(List<Feature> pointsA, List<Feature> pointsB,
                                            double thresholdA, double thresholdB,
                                            double minDistance, double maxDistance) {
        List<Feature> edges = new ArrayList<>();

        for (Feature pointA : pointsA) {
            for (Feature pointB : pointsB) {
                if (! pointA.getProperty("level").equals(pointB.getProperty("level"))) { // do not join points with different levels
                    continue;
                }
                Coord pointACoords = ((Point) pointA.getGeometry()).getCoordinates();
                Coord pointBCoords = ((Point) pointB.getGeometry()).getCoordinates();

                double dist = pointsGPSDistance(pointACoords, pointBCoords);

                if (dist > minDistance
                        && dist < maxDistance
                        && building.arePointsVisible(pointA.getProperty("level"), pointACoords, pointBCoords, thresholdA, thresholdB)) {
                    Polyline line = new Polyline();
                    line.addCoordinate(pointACoords);
                    line.addCoordinate(pointBCoords);

                    Feature feature = new Feature(line);
                    feature.addProperty("from", pointA.getProperty("uniqueId"));
                    feature.addProperty("to", pointB.getProperty("uniqueId"));
                    feature.addProperty("level", pointA.getProperty("level"));

                    edges.add(feature);
                }
            }
        }
        return edges;
    }
}
