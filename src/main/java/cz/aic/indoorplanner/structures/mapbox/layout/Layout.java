package cz.aic.indoorplanner.structures.mapbox.layout;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A wrapper for a mapbox layout
 * @see <a href="https://www.mapbox.com/mapbox-gl-js/style-spec/#layer-layout">Mapbox Layouts</a>
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Layout {
    private String iconImage;
    private Integer iconSize;
    private String textField;
    private String[] textFont;
    private Integer textSize;
    private Double[] textOffset;
    private String textAnchor;
    private String lineJoin;
    private String lineCap;
    private String visibility;

    @JsonProperty("line-join")
    public String getLineJoin() {
        return lineJoin;
    }

    @JsonProperty("line-cap")
    public String getLineCap() {
        return lineCap;
    }

    @JsonProperty("icon-image")
    public String getIconImage() {
        return iconImage;
    }

    @JsonProperty("text-field")
    public String getTextField() {
        return textField;
    }

    @JsonProperty("text-font")
    public String[] getTextFont() {
        return textFont;
    }

    @JsonProperty("text-offset")
    public Double[] getTextOffset() {
        return textOffset;
    }

    @JsonProperty("text-anchor")
    public String getTextAnchor() {
        return textAnchor;
    }

    @JsonProperty("text-size")
    public Integer getTextSize() {
        return textSize;
    }

    @JsonProperty("icon-size")
    public Integer getIconSize() {
        return iconSize;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public void setTextSize(Integer textSize) {
        this.textSize = textSize;
    }

    public void setIconImage(String iconImage) {
        this.iconImage = iconImage;
    }

    public void setTextField(String textField) {
        this.textField = textField;
    }

    public void setTextFont(String[] textFont) {
        this.textFont = textFont;
    }

    public void setTextOffset(Double[] textOffset) {
        this.textOffset = textOffset;
    }

    public void setTextAnchor(String textAnchor) {
        this.textAnchor = textAnchor;
    }

    public void setLineJoin(String lineJoin) {
        this.lineJoin = lineJoin;
    }

    public void setLineCap(String lineCap) {
        this.lineCap = lineCap;
    }

    public void setIconSize(Integer iconSize) {
        this.iconSize = iconSize;
    }
}
