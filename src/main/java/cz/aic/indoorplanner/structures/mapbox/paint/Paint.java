package cz.aic.indoorplanner.structures.mapbox.paint;

/**
 * An abstract class for mapbox paint
 * @see <a href="https://www.mapbox.com/mapbox-gl-js/style-spec/#layer-paint">Mapbox Paints</a>.
 */
public abstract class Paint {

}
