package cz.aic.indoorplanner.structures.mapbox.paint;

import cz.aic.indoorplanner.utils.ConfigUtils;

/**
 * A factory for creating an appropriate paint for each geometry type.
 */
public class PaintFactory {
    /**
     * Creates a paint for a layer with lines
     *
     * @return a paint
     */
    private static Paint createPaintForLines() {
        ConfigUtils p = ConfigUtils.getInstance();
        PathPaint paint = new PathPaint();

        paint.setLineColor(p.getProperty("path_line_color"));
        paint.setLineWidth(p.getPropertyAsInt("path_line_width"));
        paint.setLineOpacity(p.getPropertyAsDouble("path_line_opacity"));

        return paint;
    }

    /**
     * Creates a paint for a layer with nodes
     *
     * @return a paint
     */
    private static Paint createPaintForNodes() {
        PointsPaint paint = new PointsPaint();
        paint.setIconOpacity(0.0);
        paint.setTextOpacity(0.0);

        return paint;
    }

    /**
     * Creates a paint for a layer with polygons
     *
     * @return a paint
     */
    private static Paint createPaintForPolygons() {
        ConfigUtils p = ConfigUtils.getInstance();
        PolygonPaint paint = new PolygonPaint();

        paint.setFillOpacity(0.0);
        paint.setFillOutlineColor(p.getProperty("polygon_fill_outline_color"));
        paint.setFillColor(p.getProperty("polygon_fill_color"));

        return paint;
    }

    /**
     * Returns an appropriate paint for the given source form
     *
     * @param form a source form
     * @return a paint
     */
    public static Paint getPaint(String form) {
        switch (form) {
            case "points":
                return createPaintForNodes();
            case "base":
                return createPaintForPolygons();
            case "path":
                return createPaintForLines();
            case "path_points":
                return createPaintForNodes();
            case "labels":
                return createPaintForNodes();
            default:
                return null;
        }
    }
}
