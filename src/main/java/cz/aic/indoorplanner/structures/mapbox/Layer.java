package cz.aic.indoorplanner.structures.mapbox;

import cz.aic.indoorplanner.IndoorPlanner;
import cz.aic.indoorplanner.structures.mapbox.layout.Layout;
import cz.aic.indoorplanner.structures.mapbox.paint.Paint;
import org.apache.log4j.Logger;

/**
 * A mapbox layer
 * @see <a href="https://www.mapbox.com/mapbox-gl-js/style-spec/#layers">Mapbox Layers</a>.
 */
public class Layer {
    private String id;
    private String type;
    private String source;
    private Layout layout;
    private Paint paint;

    private final static Logger logger = Logger.getLogger(Layer.class);

    public Layer(String id, String type, String source, Layout layout, Paint paint) {
        this.id = id;
        this.type = type;
        this.source = source;
        this.layout = layout;
        this.paint = paint;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getSource() {
        return source;
    }

    public Layout getLayout() {
        return layout;
    }

    public Paint getPaint() {
        return paint;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setLayout(Layout layout) {
        this.layout = layout;
    }

    public void setPaint(Paint paint) {
        this.paint = paint;
    }

    /**
     * Returns a mapbox layer style which should be used for a given form.
     *
     * @param form a source form
     * @return a style of a mapbox layer to use
     */
    public static String getFormStyle(String form) {
        switch (form) {
            case "base":
                return "fill";
            case "path":
                return "line";
            case "points":
                return "symbol";
            case "path_points":
                return "symbol";
            case "labels":
                return "symbol";
            default:
                logger.warn("Unknown form " + form + ", no style available.");
                return "";
        }
    }
}
