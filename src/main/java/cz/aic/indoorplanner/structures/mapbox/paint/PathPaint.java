package cz.aic.indoorplanner.structures.mapbox.paint;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A paint for lines.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PathPaint extends Paint {
    private String lineColor;
    private int lineWidth;
    private double lineOpacity;

    @JsonProperty("line-color")
    public String getLineColor() {
        return lineColor;
    }

    public void setLineColor(String lineColor) {
        this.lineColor = lineColor;
    }

    @JsonProperty("line-width")
    public int getLineWidth() {
        return lineWidth;
    }

    public void setLineWidth(int lineWidth) {
        this.lineWidth = lineWidth;
    }

    @JsonProperty("line-opacity")
    public double getLineOpacity() {
        return lineOpacity;
    }

    public void setLineOpacity(double lineOpacity) {
        this.lineOpacity = lineOpacity;
    }
}
