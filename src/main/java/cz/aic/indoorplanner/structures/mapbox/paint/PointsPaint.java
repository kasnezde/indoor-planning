package cz.aic.indoorplanner.structures.mapbox.paint;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A paint for nodes.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PointsPaint extends Paint {
    private String textColor;
    private String iconColor;
    private double textOpacity;
    private double iconOpacity;

    @JsonProperty("text-color")
    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    @JsonProperty("icon-color")
    public String getIconColor() {
        return iconColor;
    }

    public void setIconColor(String iconColor) {
        this.iconColor = iconColor;
    }

    @JsonProperty("text-opacity")
    public double getTextOpacity() {
        return textOpacity;
    }

    public void setTextOpacity(double textOpacity) {
        this.textOpacity = textOpacity;
    }

    @JsonProperty("icon-opacity")
    public double getIconOpacity() {
        return iconOpacity;
    }

    public void setIconOpacity(double iconOpacity) {
        this.iconOpacity = iconOpacity;
    }
}
