package cz.aic.indoorplanner.structures.mapbox;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.aic.indoorplanner.structures.geojson.FeatureCollection;

import java.util.ArrayList;

/**
 * A mapbox source of the data for the map.
 * Contains a GeoJSON feature collection of geometric shapes.
 *
 *  @see <a href="https://www.mapbox.com/mapbox-gl-js/style-spec/#sources">Mapbox Sources</a>
 */
public class Source {
    @JsonIgnore
    private String id;
    @JsonIgnore
    private String levelId;
    @JsonIgnore
    private String form;
    private String type;
    private FeatureCollection data;

    // types of sources which can be used for each level
    public static String[] FORMS = {"base", "path", "points", "path_points", "labels"};

    public Source(String levelId, String form, String type) {
        this.levelId = levelId;
        this.form = form;
        this.type = type;
        this.data = new FeatureCollection(new ArrayList<>());
    }

    public String getId() {
        return levelId + "-" + form;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public FeatureCollection getData() {
        return data;
    }

    public void setData(FeatureCollection data) {
        this.data = data;
    }
}
