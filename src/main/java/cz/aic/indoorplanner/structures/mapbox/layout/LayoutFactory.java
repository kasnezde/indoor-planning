package cz.aic.indoorplanner.structures.mapbox.layout;

import cz.aic.indoorplanner.utils.ConfigUtils;

/**
 * A factory for creating an appropriate layout for each geometry type.
 */
public class LayoutFactory {
    /**
     * Creates a layout for a layer with nodes
     *
     * @return a layout
     */
    private static Layout createLayoutForPoints() {
        ConfigUtils p = ConfigUtils.getInstance();
        Layout layout = new Layout();

        layout.setTextField(p.getProperty("point_text_field"));
        layout.setIconImage(p.getProperty("point_icon_image"));
        layout.setIconSize(1);
        layout.setTextAnchor(p.getProperty("point_text_alignment"));
        layout.setTextSize(p.getPropertyAsInt("point_text_size"));
        layout.setTextOffset(new Double[] {0.0, 0.6});

        return layout;
    }

    /**
     * Creates a layout for a layer with lines
     *
     * @return a layout
     */
    private static Layout createLayoutForLines() {
        Layout layout = new Layout();

        return layout;
    }

    /**
     * Creates a layout for a layer with polygons
     *
     * @return a layout
     */
    private static Layout createLayoutForPolygons() {
        Layout layout = new Layout();

        return layout;
    }

    /**
     * Returns an appropriate layout for the given source form
     *
     * @param form a source form
     * @return a layout
     */
    public static Layout getLayout(String form) {
        switch (form) {
            case "points":
                return createLayoutForPoints();
            case "base":
                return createLayoutForPolygons();
            case "path":
                return createLayoutForLines();
            case "path_points":
                return createLayoutForPoints();
            case "labels":
                return createLayoutForPoints();
            default:
                return null;
        }
    }
}
