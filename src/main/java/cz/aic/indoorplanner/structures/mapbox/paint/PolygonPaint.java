package cz.aic.indoorplanner.structures.mapbox.paint;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A paint for polygons.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PolygonPaint extends Paint {
    private double fillOpacity;
    private String fillColor;
    private String fillOutlineColor;

    @JsonProperty("fill-color")
    public String getFillColor() {
        return fillColor;
    }

    @JsonProperty("fill-color")
    public void setFillColor(String fillColor) {
        this.fillColor = fillColor;
    }

    @JsonProperty("fill-opacity")
    public double getFillOpacity() {
        return fillOpacity;
    }

    @JsonProperty("fill-opacity")
    public void setFillOpacity(double fillOpacity) {
        this.fillOpacity = fillOpacity;
    }

    @JsonProperty("fill-outline-color")
    public String getFillOutlineColor() {
        return fillOutlineColor;
    }

    @JsonProperty("fill-outline-color")
    public void setFillOutlineColor(String fillOutlineColor) {
        this.fillOutlineColor = fillOutlineColor;
    }
}
