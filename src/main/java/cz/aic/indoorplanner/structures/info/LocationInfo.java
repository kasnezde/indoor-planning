package cz.aic.indoorplanner.structures.info;

import cz.aic.indoorplanner.structures.geojson.Point;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * A structure with information about a location (a set of buildings) loaded from json.
 */
public class LocationInfo {
    private String id;
    private List<String> languages;
    private Point location;
    private LinkedHashMap<String, String> properties;
    private String obj_type;
    private List<BuildingInfo> buildings;
    private MapInfo.Drawing drawing;

    public MapInfo.Drawing getDrawing() {
        return drawing;
    }

    public void setDrawing(MapInfo.Drawing drawing) {
        this.drawing = drawing;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

    public Point getLocation() {
        return location;
    }

    public void setLocation(Point location) {
        this.location = location;
    }

    public LinkedHashMap<String, String> getProperties() {
        return properties;
    }

    public void setProperties(LinkedHashMap<String, String> properties) {
        this.properties = properties;
    }

    public String getObj_type() {
        return obj_type;
    }

    public void setObj_type(String obj_type) {
        this.obj_type = obj_type;
    }

    public List<BuildingInfo> getBuildings() {
        return buildings;
    }

    public void setBuildings(List<BuildingInfo> buildings) {
        this.buildings = buildings;
    }

    public String getProperty(String key) {
        return properties.get(key);
    }

    public static class BuildingInfo {
        private String id;
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
