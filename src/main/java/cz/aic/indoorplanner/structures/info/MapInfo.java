package cz.aic.indoorplanner.structures.info;

import cz.aic.indoorplanner.structures.geojson.Point;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * A structure with information about a building loaded from json.
 */
public class MapInfo {
    private String id;
    private List<String> languages;
    private Drawing drawing;
    private Point location;
    private int entity_version;
    private LinkedHashMap<String, String> properties;
    private int map_version;
    private String obj_type;

    public static class Drawing {
        private int id;
        private List<Level> levels;
        private LinkedHashMap<String, String> properties;
        private String obj_type;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getProperty(String key) {
            return properties.get(key);
        }

        public LinkedHashMap<String, String> getProperties() {
            return properties;
        }

        public void setProperties(LinkedHashMap<String, String> properties) {
            this.properties = properties;
        }

        public List<Level> getLevels() {
            return levels;
        }

        public void setLevels(List<Level> levels) {
            this.levels = levels;
        }


        public String getObj_type() {
            return obj_type;
        }

        public void setObj_type(String obj_type) {
            this.obj_type = obj_type;
        }
    }

    public static class Level {
        private String id;
        private String group_id;
        private LinkedHashMap<String, String> properties;
        private String obj_type;
        private GridAxes grid_axes;
        private String building_id;

        public String getGroup_id() {
            return group_id;
        }

        public void setGroup_id(String group_id) {
            this.group_id = group_id;
        }

        public GridAxes getGrid_axes() {
            return grid_axes;
        }

        public void setGrid_axes(GridAxes grid_axes) {
            this.grid_axes = grid_axes;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProperty(String key) {
            return properties.get(key);
        }

        public LinkedHashMap<String, String> getProperties() {
            return properties;
        }

        public void setProperties(LinkedHashMap<String, String> properties) {
            this.properties = properties;
        }
        public String getObj_type() {
            return obj_type;
        }

        public void setObj_type(String obj_type) {
            this.obj_type = obj_type;
        }

        public void setBuilding_id(String building_id) {
            this.building_id = building_id;
        }

        public String getBuilding_id() {
            return building_id;
        }
    }

    public static class GridAxes {
        private Double top_left_x;
        private Double top_left_y;
        private Double top_right_x;
        private Double top_right_y;
        private Double bottom_left_x;
        private Double bottom_left_y;

        public Double getTop_left_x() {
            return top_left_x;
        }

        public void setTop_left_x(Double top_left_x) {
            this.top_left_x = top_left_x;
        }

        public Double getTop_left_y() {
            return top_left_y;
        }

        public void setTop_left_y(Double top_left_y) {
            this.top_left_y = top_left_y;
        }

        public Double getTop_right_x() {
            return top_right_x;
        }

        public void setTop_right_x(Double top_right_x) {
            this.top_right_x = top_right_x;
        }

        public Double getTop_right_y() {
            return top_right_y;
        }

        public void setTop_right_y(Double top_right_y) {
            this.top_right_y = top_right_y;
        }

        public Double getBottom_left_x() {
            return bottom_left_x;
        }

        public void setBottom_left_x(Double bottom_left_x) {
            this.bottom_left_x = bottom_left_x;
        }

        public Double getBottom_left_y() {
            return bottom_left_y;
        }

        public void setBottom_left_y(Double bottom_left_y) {
            this.bottom_left_y = bottom_left_y;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

    public Drawing getDrawing() {
        return drawing;
    }

    public void setDrawing(Drawing drawing) {
        this.drawing = drawing;
    }

    public Point getLocation() {
        return location;
    }

    public void setLocation(Point location) {
        this.location = location;
    }

    public int getEntity_version() {
        return entity_version;
    }

    public void setEntity_version(int entity_version) {
        this.entity_version = entity_version;
    }

    public LinkedHashMap<String, String> getProperties() {
        return properties;
    }

    public void setProperties(LinkedHashMap<String, String> properties) {
        this.properties = properties;
    }

    public int getMap_version() {
        return map_version;
    }

    public void setMap_version(int map_version) {
        this.map_version = map_version;
    }

    public String getObj_type() {
        return obj_type;
    }

    public void setObj_type(String obj_type) {
        this.obj_type = obj_type;
    }
}
