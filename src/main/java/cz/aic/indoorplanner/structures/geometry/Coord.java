package cz.aic.indoorplanner.structures.geometry;

/**
 * A coordinate with a longitude and a latitude.
 */
public class Coord {
    private double longitude;
    private double latitude;

    public Coord(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Coord(Double[] arr) {
//        if (arr.length != 2) {
//            throw new IllegalArgumentException("The array with coordinates should be of length 2.");
//        }
        this.longitude = arr[0];
        this.latitude = arr[1];
    }

    public double getLon() {
        return longitude;
    }

    public double getLat() {
        return latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Double[] asDoubleArray() {
        return new Double[] {longitude, latitude};
    }
}
