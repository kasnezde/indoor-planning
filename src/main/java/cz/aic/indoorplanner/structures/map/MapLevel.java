package cz.aic.indoorplanner.structures.map;

import cz.aic.indoorplanner.structures.info.MapInfo;

/**
 * Represents a level (a group of floors with the same elevation) in a building
 */
public class MapLevel {
    private String groupId;         /** an id of the group the level belongs to (usually the same elevation among buildings) */
    private String buildingId;      /** an id of the building the level belongs to */
    private String id;              /** a level id */
    private String name;            /** a level name */
    private int z;                  /** a level elevation */
    private boolean main;           /** if level is main (can be e.g. displayed first) **/

    public MapLevel(MapInfo.Level level) {
        this.name = level.getProperty("name");
        this.z = Integer.parseInt(level.getProperty("zlevel"));
        this.id = level.getId();
        this.groupId = level.getGroup_id();
        this.buildingId = level.getBuilding_id();
        this.main = Boolean.parseBoolean(level.getProperty("main"));
    }

    public String getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(String buildingId) {
        this.buildingId = buildingId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getId() {
        return id;
    }

    public boolean getMain() {
        return main;
    }

    public void setMain(boolean main) {
        this.main = main;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }
}
