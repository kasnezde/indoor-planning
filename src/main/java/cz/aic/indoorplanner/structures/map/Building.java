package cz.aic.indoorplanner.structures.map;

import cz.aic.indoorplanner.structures.geojson.Feature;
import cz.aic.indoorplanner.structures.geojson.Point;
import cz.aic.indoorplanner.structures.geojson.Polyline;
import cz.aic.indoorplanner.structures.geometry.Coord;

import java.awt.geom.Area;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cz.aic.indoorplanner.utils.GeometryUtils.pointLineDistance;
import static cz.aic.indoorplanner.utils.GeometryUtils.pointsGPSDistance;
import static cz.aic.indoorplanner.utils.GeometryUtils.segmentIntersect;

/**
 * A class wrapping up computation over building plans, e.g. doors and walls
 */
public class Building {
    private Map<String, List<List<Line2D>>> polygonPaths = new HashMap<>();
    private Map<String, List<Path2D>> polygons = new HashMap<>();
    private Map<String, List<Feature>> doors = new HashMap<>();

    /**
     * Adds a new wall to the building
     *
     * @param levelId a level where the wall is located
     * @param feature a GeoJSON line feature representing the wall
     */
    public void addWall(String levelId, Feature feature) {
        // get list of polyline coordinates
        List<Coord> coordinates = ((Polyline) feature.getGeometry()).getCoordinates();

        List<Line2D> polygonPath = new ArrayList<>();
        Path2D polygon = new Path2D.Double();

        polygon.moveTo(coordinates.get(0).getLat(), coordinates.get(0).getLon());

        for (int i = 0; i < coordinates.size() - 1; i++) {
            Coord coord = coordinates.get(i);
            Coord coordNext = coordinates.get(i+1);

            polygon.lineTo(coordNext.getLat(), coordNext.getLon());

            polygonPath.add(new Line2D.Double(coord.getLat(), coord.getLon(),
                    coordNext.getLat(), coordNext.getLon()));
        }
        polygonPaths.computeIfAbsent(levelId, v -> new ArrayList<>()).add(polygonPath);
        polygons.computeIfAbsent(levelId, v -> new ArrayList<>()).add(polygon);
    }

    /**
     * Adds a new door to the building
     *
     * @param levelId a level where the door is located
     * @param point a GeoJSON point feature representing the door
     */
    public void addDoor(String levelId, Feature point) {
        doors.computeIfAbsent(levelId, v -> new ArrayList<>()).add(point);
    }

    public double getNearestWallDistance(String levelId, Coord point) {
        double minDist = Double.MAX_VALUE;

        for (List<Line2D> polygon : polygonPaths.get(levelId)) {
            for (Line2D line : polygon) {
                double dist = Line2D.ptSegDist(line.getX1(), line.getY1(), line.getX2(), line.getY2(),
                        point.getLat(), point.getLon());

                if (dist < minDist) {
                    minDist = dist;
                }
            }
        }
        return minDist;
    }

    /**
     * Returns a distance of the nearest door on the same level in the building
     *
     * @param levelId an id of the level where the doors are located
     * @param point a point to get the distance for
     * @return a distance between the point and the nearest door in meters
     */
    public double getNearestDoorDistance(String levelId, Coord point) {
        double minDist = Double.MAX_VALUE;

        if (doors.get(levelId) == null) {
            return Double.MAX_VALUE;
        }

        for (Feature door : doors.get(levelId)) {
            double dist = pointsGPSDistance(((Point) door.getGeometry()).getCoordinates(), point);

            if (dist < minDist) {
                minDist = dist;
            }
        }
        return minDist;
    }

    /**
     * Checks if the given level of the building contains the given point
     *
     * @param levelId an id of the level
     * @param point a point to be checked
     * @return true if the point is inside any wall area of the level, false otherwise
     */
    public boolean contains(String levelId, Coord point) {
        for (Path2D polygon : polygons.get(levelId)) {
            Area area = new Area(polygon);

            if (area.contains(point.getLat(), point.getLon())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Looks for a nearest point on the wall for the given polyline.
     *
     * @param levelId a level where the polyline and the wall is located
     * @param polyLine a GeoJSON line
     * @param threshold maximum distance in which the tangent point can be located
     * @return coordinates of the nearest point if its distance is under the threshold, null otherwise
     */
    public Coord getPointCloseToAWall(String levelId, Polyline polyLine, double threshold) {
        double minDist = Double.MAX_VALUE;
        Coord minPt = null;

        for (List<Line2D> polygon : polygonPaths.get(levelId)) {
            for (Line2D line : polygon) {
                for (Coord point : polyLine.getCoordinates()) {
                    double dist = Line2D.ptSegDist(line.getX1(), line.getY1(), line.getX2(), line.getY2(),
                            point.getLat(), point.getLon());

                    if (dist < minDist) {
                        minDist = dist;
                        minPt = point;
                    }
                }
            }
        }
        if (minDist < threshold) {
            return new Coord(minPt.getLat(), minPt.getLon());
        }
        return null;
    }

    /**
     * A wrapper with zero thresholds
     */
    public boolean arePointsVisible(String levelId, Coord pointACoords, Coord pointBCoords) {
        return arePointsVisible(levelId, pointACoords, pointBCoords, 0, 0);
    }

    /**
     * Checks if point A and point B are visible.
     *
     * Constants thresholdA and thresholdB can be used for doors which usually touch a wall,
     * but still need to be visible by the other points.
     *
     * @param levelId a level where the points are located
     * @param pointACoords coordinates of the point A
     * @param pointBCoords coordinates of the point B
     * @param thresholdA a distance in which a wall can appear near a point A
     *                   and the point is still considered as visible
     * @param thresholdB a distance in which a wall can appear near a point B
     *                   and the point is still considered as visible
     * @return true if point A and B are visible (considering the thresholds), false otherwise
     */
    public boolean arePointsVisible(String levelId, Coord pointACoords, Coord pointBCoords, double thresholdA, double thresholdB) {
        for (int i = 0; i < polygonPaths.get(levelId).size(); i++) {
            List<Line2D> polygon = polygonPaths.get(levelId).get(i);

            for (Line2D line2D : polygon) {
                // return false if there is a wall in between the points
                // that is not within the distance specified by thresholds
                if (segmentIntersect(line2D, pointACoords, pointBCoords)
                        && !(pointLineDistance(line2D, pointACoords) < thresholdA
                        || pointLineDistance(line2D, pointBCoords) < thresholdB)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Returns all the doors at the given level
     *
     * @param levelId a level id
     * @return a list of doors as GeoJSON features
     */
    public List<Feature> getDoors(String levelId) {
        return doors.get(levelId);
    }
}
