package cz.aic.indoorplanner.structures.navigation;

import cz.agents.basestructures.Graph;
import cz.agents.basestructures.GraphBuilder;
import cz.aic.indoorplanner.structures.map.MapLevel;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * A wrapper for the {@link Graph} class. Uses {@link NavigationNode} as nodes and {@link NavigationEdge} as edges.
 */
public class NavigationGraph {
    private final static Logger logger = Logger.getLogger(NavigationGraph.class);

    private GraphBuilder<NavigationNode, NavigationEdge> graphBuilder;
    private Graph<NavigationNode, NavigationEdge> graph;
    private Map<Integer, MapLevel> levels;

    public NavigationGraph() {
        graphBuilder = new GraphBuilder<>();
        levels = new HashMap<>();
    }

    public Graph<NavigationNode, NavigationEdge> getGraph() {
        return graph;
    }

    public Map<Integer, MapLevel> getLevels() {
        return levels;
    }

    public void createGraph() {
        graph = graphBuilder.createGraph();
    }

    public void addNavigationNode(NavigationNode node) {
        graphBuilder.addNode(node);
    }

    public void addNavigationEdge(NavigationEdge edge) {
        try {
            graphBuilder.addEdge(edge);
        } catch (Exception e) {
            logger.warn("Cannot add edge from " + edge.getFromId() + " to " + edge.getToId());
        }
    }

    public NavigationNode getNodeById(int id) {
        return graph.getNode(id);
    }

    public Collection<NavigationNode> getNodes() {
        return graph.getAllNodes();
    }
}
