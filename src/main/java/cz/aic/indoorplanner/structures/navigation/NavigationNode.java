package cz.aic.indoorplanner.structures.navigation;

import cz.agents.basestructures.Node;
import cz.aic.indoorplanner.structures.geometry.Coord;

import java.util.LinkedHashMap;

/**
 * A class representing a node in {@link NavigationGraph}
 */
public class NavigationNode extends Node {
    private final double lat;
    private final double lon;
    private final String levelId;
    private LinkedHashMap<String, String> properties;

    public NavigationNode(int id, Coord coord, String levelId) {
        super(id, id, 0, 0, 0, 0, 0);

        this.lat = coord.getLat();
        this.lon = coord.getLon();
        this.levelId = levelId;
        this.properties = new LinkedHashMap<>();
    }

    public LinkedHashMap<String, String> getProperties() {
        return properties;
    }

    public String getProperty(String key) {
        return properties.get(key);
    }

    public void addProperty(String key, String value) {
        properties.put(key, value);
    }

    public void setProperties(LinkedHashMap<String, String> properties) {
        this.properties = properties;
    }

    public double getLatitude() {
        return lat;
    }

    public double getLongitude() {
        return lon;
    }

    public Coord getPosition() {
        return new Coord(lat, lon);
    }
    public String getLevelId() {
        return levelId;
    }
}
