package cz.aic.indoorplanner.structures.navigation;

import cz.agents.basestructures.Edge;

public class NavigationEdge extends Edge{
    private double length;
    private String object;

    public NavigationEdge(int fromId, int toId, double length, String object) {
        super(fromId, toId, (int) length); // loss of precision due to casting

        this.object = object;
        this.length = length;
    }

    public double getPreciseLength() {
        return length;
    }

    public String getObject() {
        return object;
    }
}
