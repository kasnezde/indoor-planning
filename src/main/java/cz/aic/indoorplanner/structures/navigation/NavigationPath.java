package cz.aic.indoorplanner.structures.navigation;

import java.util.ArrayList;
import java.util.List;

/**
 * A class representing a path in {@link NavigationGraph}
 */
public class NavigationPath {
    List<NavigationNode> nodes;
    List<NavigationEdge> edges;

    public NavigationPath() {
        nodes = new ArrayList<>();
        edges = new ArrayList<>();
    }

    public List<NavigationNode> getNodes() {
        return nodes;
    }

    public List<NavigationEdge> getEdges() {
        return edges;
    }

    public void addNode(NavigationNode node) {
        nodes.add(node);
    }

    public void addEdge(NavigationEdge edge) {
        edges.add(edge);
    }

    public NavigationNode getNode(int i) {
        return nodes.get(i);
    }

    public NavigationEdge getEdge(int i) {
        return edges.get(i);
    }

    public int getNodeCount() {
       return nodes.size();
    }

    public int getEdgeCount() {
        return edges.size();
    }

    @Override
    public String toString() {
        String str = "";

        for (int i = getNodeCount() - 1; i >= 0; i--) {
            NavigationNode node = getNode(i);
            str += "n id : " + node.getId() +
                    " lat: " + node.getLatitude() +
                    " lon: " + node.getLongitude();

            if (i > 0) {
                NavigationEdge edge = getEdge(i-1);
                str += "--> e "  + " from: " + edge.getFromId() +
                        " to: " + edge.getToId() +
                        " meters: " + edge.getPreciseLength();
            }
        }

        return str;
    }
}
