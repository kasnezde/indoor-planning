package cz.aic.indoorplanner.structures.geojson;

import java.util.List;

/**
 * A GeoJSON feature collection.
 */
public class FeatureCollection {
    private List<Feature> features;
    private String type;
    private Object crs;

    public FeatureCollection(List<Feature> features) {
        this.type = "FeatureCollection";
        this.features = features;
    }

    public FeatureCollection() {  // for jackson
    }

    public Object getCrs() {
        return crs;
    }

    public void setCrs(Object crs) {
        this.crs = crs;
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
