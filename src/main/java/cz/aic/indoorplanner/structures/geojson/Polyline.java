package cz.aic.indoorplanner.structures.geojson;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import cz.aic.indoorplanner.structures.geometry.Coord;

import java.util.ArrayList;
import java.util.List;

/**
 * A GeoJSON line.
 */
public class Polyline extends Geometry {
    private List<Coord> coordinates;

    public Polyline() {
        super("LineString");
        coordinates = new ArrayList<>();
    }

    public void addCoordinate(Coord coord) {
        coordinates.add(coord);
    }

    @JsonProperty("coordinates")
    public void setCoordinatesDoubleArray(Double[][] coordinatesArray) {
        for (Double[] coord : coordinatesArray) {
            coordinates.add(new Coord(coord));
        }
    }

    public void setCoordinates(List<Coord> coordinates) {
        this.coordinates = coordinates;
    }

    @JsonProperty("coordinates")
    public Double[][] getCoordinatesDoubleArray() {
        Double[][] coordinates = new Double[this.coordinates.size()][];

        for (int i = 0; i < this.coordinates.size(); i++) {
            coordinates[i] = this.coordinates.get(i).asDoubleArray();
        }
        return coordinates;
    }

    @JsonIgnore
    public List<Coord> getCoordinates() {
        return coordinates;
    }
}
