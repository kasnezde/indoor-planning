package cz.aic.indoorplanner.structures.geojson;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.LinkedHashMap;

/**
 * A GeoJSON feature.
 */
public class Feature {
    // information about subtypes needed for appropriate JSON transformation
    @JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include= JsonTypeInfo.As.PROPERTY, property="type")
    @JsonSubTypes({
            @JsonSubTypes.Type(value = Point.class, name= "Point"),
            @JsonSubTypes.Type(value = Polyline.class, name= "LineString")

    })
    private Geometry geometry;
    private LinkedHashMap<String, String> properties;
    private String type;
    private int id;

    public Feature(Geometry geometry) {
        this.type = "Feature";
        this.geometry = geometry;
        this.properties =  new LinkedHashMap<>();
    }

    public Feature() {  // for jackson
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public LinkedHashMap<String, String> getProperties() {
        return properties;
    }

    public boolean hasProperty(String key) {
        return properties.containsKey(key);
    }

    public String getProperty(String key) {
        return properties.get(key);
    }

    public void addProperty(String key, String value) {
        properties.put(key, value);
    }

    public void setProperties(LinkedHashMap<String, String> properties) {
        this.properties = properties;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
