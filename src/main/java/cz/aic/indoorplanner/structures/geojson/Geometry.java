package cz.aic.indoorplanner.structures.geojson;

/**
 * A GeoJSON geometry.
 */
public abstract class Geometry {
    String type;

    public Geometry(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
