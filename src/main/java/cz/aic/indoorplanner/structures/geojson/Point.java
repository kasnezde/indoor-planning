package cz.aic.indoorplanner.structures.geojson;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import cz.aic.indoorplanner.structures.geometry.Coord;

/**
 * A GeoJSON point.
 */
public class Point extends Geometry {
    private Coord coordinates;

    @JsonIgnore
    private String name;

    public Point() {
        super("Point");
    }

    public Point(Coord coordinates) {
        super("Point");
        this.coordinates = coordinates;
        this.name = "Point";
    }

    @JsonProperty("coordinates")
    public Double[] getCoordinatesAsDoubleArray() {
        return coordinates.asDoubleArray();
    }

    @JsonProperty("coordinates")
    public void setCoordinates(Double[] coordinatesArray) {
        this.coordinates = new Coord(coordinatesArray);
    }

    public Coord getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coord coordinates) {
        this.coordinates = coordinates;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
