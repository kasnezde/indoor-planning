package cz.aic.indoorplanner.structures.analysis;

public enum InstructionType {
    TURN_LEFT,
    TURN_RIGHT,
    GO_STRAIGHT,
    DOOR,
    STAIRS,
    ELEVATOR
}
