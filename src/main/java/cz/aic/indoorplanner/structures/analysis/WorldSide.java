package cz.aic.indoorplanner.structures.analysis;

public enum WorldSide {
    N, NE, E, SE, S, SW, W, NW
}
