package cz.aic.indoorplanner.structures.analysis;

public class Instruction {
    private InstructionType type;
    private double dist;
    private String from;
    private String to;
    private WorldSide side;

    public Instruction(InstructionType type) {
        this.type = type;
    }

    public Instruction(InstructionType type, WorldSide side, double dist) {
        this.type = type;
        this.dist = dist;
        this.side = side;
    }

    public Instruction(InstructionType type, String from, String to) {
        this.type = type;
        this.from = from;
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public InstructionType getType() {
        return type;
    }

    public void setType(InstructionType type) {
        this.type = type;
    }

    public double getDist() {
        return dist;
    }

    public void setDist(double dist) {
        this.dist = dist;
    }

    public WorldSide getSide() {
        return side;
    }

    public void setSide(WorldSide side) {
        this.side = side;
    }
}
