package cz.aic.indoorplanner.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.log4j.Logger;

import java.io.IOException;

/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
public class JSONUtils {
        private static final Logger logger = Logger.getLogger(JSONUtils.class);
        private static ObjectMapper mapper = new ObjectMapper();

        static {
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        }

        /**
         * Convert Java object to JSON
         *
         * @param object Java object
         * @return Java object as JSON
         */
        public static String javaObjectToJSON(Object object) {
            try {
                mapper.enable(SerializationFeature.INDENT_OUTPUT);
                String s = mapper.writeValueAsString(object);
                mapper.disable(SerializationFeature.INDENT_OUTPUT);
                return s;
            } catch (JsonProcessingException e) {
                logger.warn("Cannot convert " + object.getClass().getSimpleName() + " to JSON string.", e);
                return null;
            }
        }

        /**
         * Convert Java object to one line JSON
         *
         * @param object Java object
         * @return Java object as JSON
         */
        public static String javaObjectToOneLineJSON(Object object) {

            try {
                return mapper.writeValueAsString(object);
            } catch (JsonProcessingException e) {
                logger.warn("Cannot convert " + object.getClass().getSimpleName() + " to JSON string.", e);
                return null;
            }
        }

        public static <T> T convertJSONStringToDesiredObject(String json, TypeReference<T> clazz) {

            // JSON -> JAVA
            T obj = null;
            try {
                obj = mapper.readValue(json, clazz);
            } catch (IOException e) {
                logger.error("Exception in converting json " + ((json == null) ?
                        "null" :
                        json.substring(0, Math.min(json.length(), 100))) + " to JAVA class " + clazz.toString() + ". ", e);
            }
            return obj;

        }

    }

