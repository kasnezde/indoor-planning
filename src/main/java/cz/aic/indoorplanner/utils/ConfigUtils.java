package cz.aic.indoorplanner.utils;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Properties;

/**
 * Global settings
 */
public class ConfigUtils {
    private final static Logger logger = Logger.getLogger(ConfigUtils.class);

    private static final String PROPERTY_FILE = "config.properties";
    private static ConfigUtils instance = null;
    private static Properties prop;

    public String getProperty(String key) {
        return prop.getProperty(key);
    }

    public Double getPropertyAsDouble(String key) {
        return Double.parseDouble(getProperty(key));
    }

    public Integer getPropertyAsInt(String key) {
        return Integer.parseInt(getProperty(key));
    }

    public static ConfigUtils getInstance() {
        if (instance == null) {
            instance = new ConfigUtils();
        }
        return instance;
    }

    private ConfigUtils() {
        prop = new Properties();
        try {
            prop.load(IOUtils.readFileFromResources(PROPERTY_FILE));
        } catch (IOException e) {
            logger.error("Cannot load global settings from " + PROPERTY_FILE);
            e.printStackTrace();
        }
    }
}
