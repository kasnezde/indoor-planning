package cz.aic.indoorplanner.utils;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A class for input/output operations.
 */
public class IOUtils {
	private final static Logger logger = Logger.getLogger(IOUtils.class);

    /**
     * Reads a text file to a string
     *
     * @param filename a path to the file
     * @return a content of the file in a string
     */
	public static String readTextFile(String filename) {
		String content = null;
		try {
			content = new String(Files.readAllBytes(Paths.get(filename)));
		} catch (Exception e) {
			logger.error("Cannot read file " + filename);
			e.printStackTrace();
		}
		return content;
	}

    /**
     * Creates a text file from a string
     *
     * @param filename a path to the file
     * @param str a string to be saved
     */
	public static void writeTextFile(String filename, String str) {
		List<String> lines = Arrays.asList(str.split("\\n"));

		Path file = Paths.get(filename);
		try {
			Files.write(file, lines, Charset.forName("UTF-8"));
		} catch (IOException e) {
			logger.error("Cannot save file " + filename);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    private static String getStringFromInputStream(InputStream is) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        String line;

        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();

    }
	public static String readTextFileFromResources(String path) {
        InputStream resource = readFileFromResources(path);
        String str = getStringFromInputStream(resource);

        return str;
    }

    /**
     * Loads a resource using a ClassLoader
     *
     * @param path a path to the resource
     * @return a resource as an input stream
     */
	public static InputStream readFileFromResources(String path) {
		return IOUtils.class.getClassLoader().getResourceAsStream(path);
	}

}
