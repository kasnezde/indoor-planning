package cz.aic.indoorplanner.utils;

import cz.aic.indoorplanner.structures.geometry.Coord;

import java.awt.geom.Line2D;

import static cz.agents.geotools.DistanceUtil.computeGreatCircleDistance;

/**
 * Provides methods for computations with geometry
 */
public class GeometryUtils {
    /**
     * Checks if a line segment intersects with another. The second line segment
     * is created by joining two points.
     *
     * @param line2D the first line segment
     * @param segStart coordinates of a point denoting a start of the second line segment
     * @param segEnd coordinates of a point denoting an end of the second line segment.
     * @return true if line segments intersects, false otherwise
     */
    public static boolean segmentIntersect(Line2D line2D, Coord segStart, Coord segEnd) {
        return Line2D.linesIntersect(line2D.getX1(), line2D.getY1(), line2D.getX2(), line2D.getY2(),
                segStart.getLat(), segStart.getLon(),
                segEnd.getLat(), segEnd.getLon());
    }

    /**
     * Computes a great circle distance between two coordinates.
     *
     * @param pointACoords coordinates of the first point
     * @param pointBCoords coordinates of the second point
     * @return a distance between the points in meters
     */
    public static double pointsGPSDistance(Coord pointACoords, Coord pointBCoords) {
        return computeGreatCircleDistance(pointACoords.getLat(), pointACoords.getLon(),
                pointBCoords.getLat(), pointBCoords.getLon());
    }

    /**
     * Computes a distance between a point and the nearest point on a line.
     *
     * @param line2D a line
     * @param coord coordinates of a point
     * @return distance between a point and a line (measured along the perpendicular)
     */
    public static double pointLineDistance(Line2D line2D, Coord coord) {
        return Line2D.ptLineDist(line2D.getX1(), line2D.getY1(), line2D.getX2(), line2D.getY2(),
                coord.getLat(), coord.getLon());
    }
}
