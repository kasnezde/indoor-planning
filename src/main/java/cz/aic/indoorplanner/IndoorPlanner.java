package cz.aic.indoorplanner;

import com.fasterxml.jackson.core.type.TypeReference;
import cz.aic.indoorplanner.api.response.MapDataResponse;
import cz.aic.indoorplanner.api.response.ListResponse;
import cz.aic.indoorplanner.map.MapBuilder;
import cz.aic.indoorplanner.structures.analysis.Instruction;
import cz.aic.indoorplanner.structures.analysis.InstructionType;
import cz.aic.indoorplanner.structures.analysis.WorldSide;
import cz.aic.indoorplanner.structures.map.MapLevel;
import cz.aic.indoorplanner.map.Navigator;
import cz.aic.indoorplanner.structures.geojson.Feature;
import cz.aic.indoorplanner.structures.geojson.FeatureCollection;
import cz.aic.indoorplanner.structures.geojson.Point;
import cz.aic.indoorplanner.structures.geometry.Coord;
import cz.aic.indoorplanner.structures.info.LocationInfo;
import cz.aic.indoorplanner.structures.info.MapInfo;
import cz.aic.indoorplanner.structures.mapbox.Layer;
import cz.aic.indoorplanner.structures.navigation.NavigationEdge;
import cz.aic.indoorplanner.structures.navigation.NavigationNode;
import cz.aic.indoorplanner.structures.navigation.NavigationPath;
import cz.aic.indoorplanner.utils.ConfigUtils;
import cz.aic.indoorplanner.utils.IOUtils;
import cz.aic.indoorplanner.utils.JSONUtils;
import jdk.nashorn.internal.runtime.Undefined;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cz.agents.geotools.DistanceUtil.computeGreatCircleDistance;

/**
 * Main class taking care of planning tasks and manipulating with resources.
 */
public class IndoorPlanner {
    private final static Logger logger = Logger.getLogger(IndoorPlanner.class);

    private String mapDirectory;
    private LocationInfo locationInfo;
    private List<MapInfo> mapInfos;
    private Navigator navigator;
    private MapBuilder mb;
    private String locationId;

    public IndoorPlanner() {
        ConfigUtils p = ConfigUtils.getInstance();

        mb = new MapBuilder();
        locationId = null;
        mapInfos = new ArrayList<>();
        mapDirectory = p.getProperty("map_directory");
    }

    /**
     * Checks if a map is loaded into the planner
     *
     * @return true if a map is loaded, false otherwise
     */
    public boolean isMapLoaded() {
        return locationId != null;
    }

    /**
     * Checks if a map of a given id is loaded into the planner
     *
     * @param id a map id
     * @return true if a map with a given id is loaded, false otherwise
     */
    public boolean isMapIdLoaded(String id) {
        return locationId != null && locationId.equals(id);
    }

    /**
     * Loads a map with a given id into the planner. The map should be located in the directory named {@code id} with
     * a parent directory specified in the config.
     *
     * @param id a map id
     */
    public void loadMap(String id) {
        mb = new MapBuilder();
        navigator = new Navigator();

        this.locationId = id;

        // info of the whole location
        locationInfo = loadLocationInfo(getLocationDir());
        List<LocationInfo.BuildingInfo> buildingInfos = locationInfo.getBuildings();

        // info of each building
        for (LocationInfo.BuildingInfo buildingInfo : buildingInfos) {
            MapInfo mapInfo = loadMapInfo(getLocationDir() + "/" + buildingInfo.getId());
            mapInfos.add(mapInfo);
        }
        // level info
        loadLevels();

        // geometries
        loadLevelGeometries();

        // navigation graph
        loadNavigation();

        logger.info("Map with id "+id+" loaded.");
    }

    /**
     * Finds a path between two nodes
     *
     * @param fromNodeId id of the start node
     * @param toNodeId id of the target node
     * @return a set of nodes and edges which forms the shortest path between the start node and the target node
     */
    public NavigationPath findPath(int fromNodeId, int toNodeId, boolean accessibility) {
        return navigator.findPath(fromNodeId, toNodeId, accessibility);
    }

    /**
     * Creates a reduced version of the path. See docs of the called method for more details.
     *
     * @param path a path to be reduced
     * @return a reduced version of the given path
     */
    public NavigationPath reducePath(NavigationPath path) {
        return navigator.reducePath(mb.getBuilding(), path);
    }

    /**
     * Finds an id of the closest node in the navigation graph for the given coordinates.
     *
     * @param lat a latitude
     * @param lon a longitude
     * @param groupId an id of the level group
     * @param maxDist a maximal distance of the node from the coordinates
     * @return an id of the closest node in the navigation graph or -1, if the closest node is beyond {@code maxDist}
     */
    public int findClosestPointId(double lat, double lon, String groupId, double maxDist) {
        int closestPointId = -1;
        double minDist = Double.MAX_VALUE;

        List<String> levelIds = new ArrayList<>();

        // we have to get levels in the given group
        // TODO retrieve the levels directly
        for (MapLevel level : mb.getSortedLevels()) {
            if (level.getGroupId().equals(groupId)) {
                levelIds.add(level.getId());
            }
        }

        for (NavigationNode node : navigator.getNodes()) {
            // we cannot separate nodes from the navigation graph by levels, they have to be filtered
            if (!levelIds.contains(node.getLevelId())) {
                continue;
            }
            double dist = computeGreatCircleDistance(lat, lon, node.getLatitude(), node.getLongitude());

            if (dist < minDist) {
                minDist = dist;
                closestPointId = node.getId();
            }
        }
        if (minDist > maxDist) {
            return -1;
        }
        return closestPointId;
    }

    /**
     * Loads a JSON with map info to a Java wrapper
     *
     * @param mapDir a map directory
     * @return a map info object
     */
    public static MapInfo loadMapInfo(String mapDir) {
        final String MAP_INFO_FILENAME = "map-info.json";

        String filename = mapDir + "/" + MAP_INFO_FILENAME;
        MapInfo mapInfo = null;

        try {
            String jsonStr = IOUtils.readTextFileFromResources(filename);
            mapInfo = JSONUtils.convertJSONStringToDesiredObject(jsonStr,
                new TypeReference<MapInfo>() {});
            return mapInfo;
        } catch (NullPointerException e) {
            logger.error("Cannot load map information from " + filename + ".");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapInfo;
    }

    /**
     * Loads a JSON with map info to a Java wrapper
     *
     * @param mapDir a location directory
     * @return a location info object
     */
    public static LocationInfo loadLocationInfo(String mapDir) {
        final String LOCATION_INFO_FILENAME = "location-info.json";

        String filename = mapDir + "/" + LOCATION_INFO_FILENAME;
        LocationInfo locationInfo = null;

        try {
            String jsonStr = IOUtils.readTextFileFromResources(filename);
            locationInfo = JSONUtils.convertJSONStringToDesiredObject(jsonStr,
                    new TypeReference<LocationInfo>() {});
            return locationInfo;
        } catch (NullPointerException e) {
            logger.error("Cannot load location information from " + filename + ".");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return locationInfo;
    }

    /**
     * Loads a JSON with level geometry information to a Java wrapper
     *
     * @param buildingId a building of the level
     * @param levelId a level for which the JSON should be loaded
     * @return a wrapper for the level geometry information
     */
    private FeatureCollection loadLevelGeometry(String buildingId, String levelId) {
        FeatureCollection levelGeometryObject = null;
        String filename = getLocationDir() + "/" + buildingId + "/" + levelId +  ".json";

        try {
            String jsonStr = IOUtils.readTextFileFromResources(filename);
            levelGeometryObject = JSONUtils.convertJSONStringToDesiredObject(jsonStr,
                new TypeReference<FeatureCollection>() {});
        } catch (NullPointerException e) {
            logger.warn("Cannot load level geometry from " + filename + ".");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return levelGeometryObject;
    }

    /**
     * Loads level geometries to the MapBuilder.
     */
    private void loadLevelGeometries() {
        for (MapInfo mapInfo : mapInfos) {
            String buildingId = mapInfo.getId();

            for (MapInfo.Level level : mapInfo.getDrawing().getLevels()) {
                String levelId = level.getId();
                FeatureCollection featureCollection = loadLevelGeometry(buildingId, levelId);

                if (featureCollection != null) {
                    mb.addLevelGeometry(levelId, featureCollection);
                    loadLabels(levelId, featureCollection);
                }
            }
        }
    }

    /**
     * Loads room labels to the MapBuilder.
     *
     * @param levelId an id of the working level
     * @param featureCollection a collection of geometry features of the level
     */
    private void loadLabels(String levelId, FeatureCollection featureCollection) {
        for (Feature feature : featureCollection.getFeatures()) {
            if (feature.getProperty("SubClasses").contains("AcDbMText")) { // "AcDbMText" should denote room labels
                String label = feature.getProperty("Text");
                mb.addLabel(levelId, (Point) feature.getGeometry(), label);
            }
        }
    }

    /**
     * Loads files with navigation graph to the Navigator
     */
    private void loadNavigation() {
        for (MapInfo mapInfo : mapInfos) {
            String buildingId = mapInfo.getId();

            // load navigation files of each level
            for (MapInfo.Level level : mapInfo.getDrawing().getLevels()) {
                String filename = getLocationDir() + "/" + buildingId + "/" + level.getId() + ".nav.json";
                navigator.addNavigationFile(filename);

            }
            // load a navigation file of each building (connections between levels)
            String filename = getLocationDir() + "/" + buildingId + "/" + "map.nav.json";
            navigator.addNavigationFile(filename);
        }
        // load a navigation file of a location (connections between buildings)
        String filename = getLocationDir() + "/" + "location.nav.json";
        navigator.addNavigationFile(filename);

        navigator.createGraph();
    }

    /**
     * Load levels specified in the building info file.
     */
    private void loadLevels() {
        for (MapInfo mapInfo : mapInfos) {
            for (MapInfo.Level level : mapInfo.getDrawing().getLevels()) {
                MapLevel mapLevel = new MapLevel(level);

                mb.addNavigationLevel(mapLevel);
            }
        }

        MapLevel mapLevel = new MapLevel(locationInfo.getDrawing().getLevels().get(0));
        mb.addNavigationLevel(mapLevel);
    }

    /**
     * Returns the root directory of the current location.
     *
     * @return location directory
     */
    public String getLocationDir() {
        return mapDirectory + "/" + locationId;
    }

    /**
     * Returns a server response with all data required to manipulate and display map sources
     *
     * @return response with current map data
     */
    public MapDataResponse getMapResponse() {
        final String[] forms = {"points", "base", "labels"}; // source forms to retrieve from the MapBuilder

        Map<String, FeatureCollection> sources = mb.getSources(forms);
        List<Layer> layers = mb.getLayers(forms);
        List<MapLevel> levels = mb.getSortedLevels();

        Coord startLocation =  locationInfo.getLocation().getCoordinates();
        String zoom = locationInfo.getProperty("default_zoom");

        MapDataResponse response = new MapDataResponse();

        response.setSources(sources);
        response.setLayers(layers);

        response.addVariable("levels", levels);
        response.addVariable("zoom", zoom);
        response.addVariable("startLatitude", startLocation.getLat());
        response.addVariable("startLongitude", startLocation.getLon());

        Map<String, String> buildingIds = new HashMap<>();

        for (LocationInfo.BuildingInfo buildingInfo : locationInfo.getBuildings()) {
            buildingIds.put(buildingInfo.getId(), buildingInfo.getName()) ;
        }
        response.addVariable("buildingIds", buildingIds);

        return response;
    }

    /**
     * Returns a server response with all data required to manipulate and display path sources
     *
     * @return response with current path data
     */
    public MapDataResponse getPathResponse() {
        final String[] forms = {"path", "path_points"};  // source forms to retrieve from the MapBuilder

        NavigationPath path = mb.getPath();

        Map<String, FeatureCollection> sources = mb.getSources(forms);
        List<Layer> layers = mb.getLayers(forms);

        MapDataResponse response = new MapDataResponse();
        response.setSources(sources);
        response.setLayers(layers);
        response.addVariable("distance", navigator.calculatePathLength(path));

        List<Instruction> instructions = analyzePath(path);
        response.addVariable("instructions", instructions);

        return response;
    }

    private List<Instruction> analyzePath(NavigationPath path) {
        List<NavigationEdge> edges = path.getEdges();
        List<NavigationNode> nodes = path.getNodes();
        List<Instruction> instructions = new ArrayList<>();

        for (int i = nodes.size()-1; i >= 1; i--) { // path is backwards
            NavigationNode node = nodes.get(i);
            NavigationNode nextNode = nodes.get(i-1);
            NavigationEdge edge = edges.get(i-1);
            double dist = edge.getPreciseLength();
            double angle = Math.atan2(nextNode.getLatitude() - node.getLatitude(),
                    nextNode.getLongitude() - node.getLongitude());
            angle = angle < 0 ? angle + 2 * Math.PI : angle;
            double degrees = Math.toDegrees(angle);

            WorldSide side = getWorldSide(degrees);

            // go straight
            if (!instructions.isEmpty()) {
                Instruction prevInstr = instructions.get(instructions.size() - 1);
                if (prevInstr.getType().equals(InstructionType.GO_STRAIGHT) && prevInstr.getSide().equals(side)) {
                    instructions.get(instructions.size() - 1).setDist(instructions.get(instructions.size() - 1).getDist() + dist);
                } else if (dist > 3) {
                    if (prevInstr.getType().equals(InstructionType.GO_STRAIGHT)) {
                        Instruction turn = getTurn(prevInstr.getSide(),side);
                        if (turn != null) {
                            instructions.add(turn);
                        }
                    }
                    instructions.add(new Instruction(InstructionType.GO_STRAIGHT, side, dist));
                }
            } else if (dist > 2.5) {
                instructions.add(new Instruction(InstructionType.GO_STRAIGHT, side, dist));
            }

            // stairs / elevator
            if (edge.getObject() != null) {
                if (edge.getObject().equals("stairs")) {
                    instructions.add(new Instruction(InstructionType.STAIRS, mb.getLevelById(node.getLevelId()).getGroupId(), mb.getLevelById(nextNode.getLevelId()).getGroupId()));
                } else if (edge.getObject().equals("elevator")) {
                    if (!instructions.isEmpty() && instructions.get(instructions.size() - 1).getType().equals(InstructionType.ELEVATOR)) {
                        instructions.get(instructions.size() - 1).setTo(mb.getLevelById(nextNode.getLevelId()).getGroupId());
                    } else {
                        instructions.add(new Instruction(InstructionType.ELEVATOR, mb.getLevelById(node.getLevelId()).getGroupId(), mb.getLevelById(nextNode.getLevelId()).getGroupId()));
                    }
                }
            }

            // door
            if (nextNode.getProperty("placement").equals("door")) {
                instructions.add(new Instruction(InstructionType.DOOR));
            }
        }
        return instructions;
    }

    private int mod(int num, int modulus) {
        num = num % modulus;

        if (num < 0) {
            num += modulus;
        }
        return num;
    }

    private Instruction getTurn(WorldSide side, WorldSide nextSide) {
        if (nextSide.ordinal() == mod(side.ordinal() + 2, 8) || nextSide.ordinal() ==  mod(side.ordinal() + 3, 8)) {
            return new Instruction(InstructionType.TURN_RIGHT);
        } else if (nextSide.ordinal() ==  mod(side.ordinal() - 2, 8) || nextSide.ordinal() ==  mod(side.ordinal() - 3, 8)) {
            return new Instruction(InstructionType.TURN_LEFT);
        }
        return null;
    }

    private WorldSide getWorldSide(double degrees) {
        if ((degrees >= 337.5 & degrees < 360) || (degrees >= 0 && degrees < 22.5)) {
            return WorldSide.E;
        } else if (degrees >= 22.5 && degrees < 67.5) {
            return WorldSide.NE;
        } else if (degrees >= 67.5 && degrees < 112.5) {
            return WorldSide.N;
        }  else if (degrees >= 112.5 && degrees < 157.5) {
            return WorldSide.NW;
        } else if (degrees >= 157.5 && degrees < 202.5) {
            return WorldSide.W;
        } else if (degrees >= 202.5 && degrees < 247.5) {
            return WorldSide.SW;
        } else if (degrees >= 247.5 && degrees < 292.5) {
            return WorldSide.S;
        } else if (degrees >= 292.5 && degrees < 337.5) {
            return WorldSide.SE;
        } else {
            logger.error("World side for " + degrees + " undefined.");
            return WorldSide.N;
        }
    }

    public ListResponse getPathInfoResponse() {
        ListResponse response = new ListResponse();

        NavigationPath path = mb.getPath();

        List<Instruction> instructions = analyzePath(path);
        response.addObject(instructions);
        return response;
    }

    /**
     * Returns a server response with information about locations
     *
     * @return response with location infos
     */
    public ListResponse getLocationListResponse() {
        File file = new File(mapDirectory);
        String[] directories = file.list((current, name) -> new File(current, name).isDirectory());

        ListResponse mapListResponse = new ListResponse();

        assert directories != null;
        for (String dir : directories) {
            mapListResponse.addObject(loadLocationInfo(mapDirectory + "/" + dir));
        }
        return mapListResponse;
    }

    /**
     * Returns a server response with information about rooms and places
     *
     * @return response with place infos
     */
    public ListResponse getPlacesListResponse() {
        ListResponse response = new ListResponse();
        final String[] forms = {"labels"};

        Map<String, FeatureCollection> sources = mb.getSources(forms);

        for (FeatureCollection label : sources.values()) {
            response.addObject(label);
        }
        return response;
    }

    /**
     * Adds or replaces the path sources in order to display a new path on the map
     *
     * @param path a path to be set
     */
    public void setPath(NavigationPath path) {
        final String[] forms = {"path", "path_points"};

        mb.clearSources(forms);
        mb.addPath(path);
    }

    /**
     * Returns a navigation node by its id in the navigation graph
     *
     * @param id a node id
     * @return a node with the given id
     */
    public NavigationNode getNodeById(int id) {
        return navigator.getNodeById(id);
    }
}
