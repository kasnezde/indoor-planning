package cz.aic.indoorplanner.api.response;

import cz.aic.indoorplanner.structures.geojson.FeatureCollection;
import cz.aic.indoorplanner.structures.mapbox.Layer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A server response used to send map data
 */
public class MapDataResponse {
    Map<String, FeatureCollection> sources;
    List<Layer> layers;
    Map<String, Object> variables;

    public List<Layer> getLayers() {
        return layers;
    }

    public void setLayers(List<Layer> layers) {
        this.layers = layers;
    }

    public MapDataResponse() {
        this.variables = new HashMap<>();
    }

    public Map<String, FeatureCollection> getSources() {
        return sources;
    }

    public void setSources(Map<String, FeatureCollection> sources) {
        this.sources = sources;
    }

    public Map<String, Object> getVariables() {
        return variables;
    }

    public void setVariables(Map<String, Object> variables) {
        this.variables = variables;
    }

    public void addVariable(String key, Object value) {
        variables.put(key, value);
    }
}
