package cz.aic.indoorplanner.api.response;

import java.util.ArrayList;
import java.util.List;

/**
 * A server response used to send a list of objects
 */
public class ListResponse {
    List<Object> objectList;

    public ListResponse() {
        objectList = new ArrayList<>();
    }

    public List<Object> getObjectList() {
        return objectList;
    }

    public void setObjectList(List<Object> objectList) {
        this.objectList = objectList;
    }

    public void addObject(Object object) {
        objectList.add(object);
    }
}
