package cz.aic.indoorplanner.api;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.ext.*;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

@Provider
public class GZIPInterceptor implements ReaderInterceptor, WriterInterceptor {

	@Override
	public Object aroundReadFrom(ReaderInterceptorContext context) throws IOException, WebApplicationException {
		List<String> header = context.getHeaders().get("Content-Encoding");

		// decompress gzip stream only
		if (header != null && header.contains("gzip"))
			context.setInputStream(new GZIPInputStream(context.getInputStream()));

		return context.proceed();
	}

	@Override
	public void aroundWriteTo(WriterInterceptorContext context) throws IOException, WebApplicationException {
		final OutputStream outputStream = context.getOutputStream();

		context.setOutputStream(new GZIPOutputStream(outputStream));
		context.getHeaders().add("Content-Encoding", "gzip");
		context.proceed();
	}
}
