package cz.aic.indoorplanner.api;

import org.apache.log4j.Logger;
import cz.aic.indoorplanner.IndoorPlanner;

import javax.servlet.ServletContextEvent;
import javax.servlet.annotation.WebListener;

@WebListener
public class PlannerServletContextListener implements javax.servlet.ServletContextListener {
	private static final Logger logger = Logger.getLogger(PlannerServletContextListener.class);
    public static IndoorPlanner indoorPlanner;

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		logger.info("Call contextInitialized(...) method.");

        indoorPlanner = new IndoorPlanner();
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		logger.info("Call contextDestroyed(...) method.");

	}

}
