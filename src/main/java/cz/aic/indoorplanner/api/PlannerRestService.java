package cz.aic.indoorplanner.api;

import cz.aic.indoorplanner.api.response.MapDataResponse;
import cz.aic.indoorplanner.api.response.ListResponse;
import cz.aic.indoorplanner.structures.geometry.Coord;
import cz.aic.indoorplanner.structures.navigation.NavigationNode;
import cz.aic.indoorplanner.structures.navigation.NavigationPath;
import cz.aic.indoorplanner.utils.ConfigUtils;
import io.swagger.annotations.*;
import org.apache.log4j.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static cz.aic.indoorplanner.api.PlannerServletContextListener.indoorPlanner;

@Path("/v1")
@Api(value = "/v1/planner", description = "Access to planner component")
public class PlannerRestService {
    private static final Logger logger = Logger.getLogger(PlannerRestService.class);
    private ConfigUtils p = ConfigUtils.getInstance();

	@GET
	@Path("/planner")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Search for a path between two points.")
	@ApiResponses({ @ApiResponse(code = 200, message = "Path successfully found.", response = MapDataResponse.class),
			@ApiResponse(code = 500, message = "Failed to find path.")})
	public Response getPath(
            @ApiParam(value = "Start location latitude", required = true) @QueryParam("startLat") double startLat,
            @ApiParam(value = "Start location longitude", required = true) @QueryParam("startLon") double startLon,
            @ApiParam(value = "Level group of the start location", required = true) @QueryParam("startLevelGroupId") String startLevelGroupId,
            @ApiParam(value = "Target location latitude", required = true) @QueryParam("endLat") double endLat,
			@ApiParam(value = "Target location longitude", required = true) @QueryParam("endLon") double endLon,
            @ApiParam(value = "Level group of the target location", required = true) @QueryParam("endLevelGroupId") String endLevelGroupId,
            @ApiParam(value = "Accessibility mode", required = true) @QueryParam("accessibility") boolean accessibility)  {

        logger.info("getPath(" + startLat + ", " + startLon +  ", " + endLat + ", " + endLon  + " ) called");
		Response.Status statusCode;
        NavigationPath path = null;

        int fromId = indoorPlanner.findClosestPointId(startLat, startLon, startLevelGroupId, p.getPropertyAsDouble("max_dist_from_nearest_point"));
        int toId = indoorPlanner.findClosestPointId(endLat, endLon, endLevelGroupId, p.getPropertyAsDouble("max_dist_from_nearest_point"));

        if (fromId != -1 && toId !=-1) {
            logger.info("planning path from " + fromId + " to " + toId);
            path = indoorPlanner.findPath(fromId, toId, accessibility);
        }
        if (path != null) {
            path = indoorPlanner.reducePath(path); // used to make path prettier, skipping over unnecessary nodes
            indoorPlanner.setPath(path);           // create necessary sources and layers inside a current map
            MapDataResponse response = indoorPlanner.getPathResponse();

			return Response.ok(response, MediaType.APPLICATION_JSON_TYPE).build();
		} else {
			statusCode = Response.Status.NOT_FOUND;
			return Response.status(statusCode).build();
		}
	}

    @GET
    @Path("/location/nearest")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get nearest point on the grid.")
    @ApiResponses({ @ApiResponse(code = 200, message = "Point found.", response = Coord.class),
            @ApiResponse(code = 500, message = "No point near the location.")})
    public Response getNearestPoint(
            @ApiParam(value = "Location latitude", required = true) @QueryParam("lat") double lat,
            @ApiParam(value = "Location longitude", required = true) @QueryParam("lon") double lon,
            @ApiParam(value = "Level group of the location", required = true) @QueryParam("levelGroupId") String levelGroupId) {

        logger.info("getNearestPoint(" + lat + ", " + lon + " ) called");
        Response.Status statusCode;

        int id = indoorPlanner.findClosestPointId(lat, lon, levelGroupId, p.getPropertyAsDouble("max_dist_from_nearest_point"));

        if (id != -1) {
            NavigationNode node = indoorPlanner.getNodeById(id);
            Coord pos = node.getPosition();
            return Response.ok(pos, MediaType.APPLICATION_JSON_TYPE).build();
        } else {
            statusCode = Response.Status.NOT_FOUND;
            return Response.status(statusCode).build();
        }
    }

    @GET
    @Path("/location")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get a location plan.")
    @ApiResponses({ @ApiResponse(code = 200, message = "Location found.", response = MapDataResponse.class),
            @ApiResponse(code = 500, message = "Failed to find the location.")})
    public Response getLocation(
            @ApiParam(value = "Location id", required = true) @QueryParam("id") String id) {

        logger.info("getLocation("+ id +") called");
        Response.Status statusCode;

        try {
            if (!indoorPlanner.isMapIdLoaded(id)) {
                indoorPlanner.loadMap(id);
            }
            MapDataResponse response = indoorPlanner.getMapResponse();

            return Response.ok(response, MediaType.APPLICATION_JSON_TYPE).build();
        } catch (Exception e){
            e.printStackTrace();
            statusCode = Response.Status.NOT_FOUND;
            return Response.status(statusCode).build();
        }
    }

    @GET
    @Path("/building/places")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get a list of places in a building.")
    @ApiResponses({ @ApiResponse(code = 200, message = "List of places found.", response = ListResponse.class),
            @ApiResponse(code = 500, message = "Failed to find the list of places.")})
    public Response getBuildingPlaces(
            @ApiParam(value = "Building id", required = true) @QueryParam("id") String id) {

        logger.info("getBuildingPlaces("+ id +") called");
        Response.Status statusCode;

        if (indoorPlanner.isMapLoaded()) {
            ListResponse response = indoorPlanner.getPlacesListResponse();
            return Response.ok(response, MediaType.APPLICATION_JSON_TYPE).build();
        } else {
            statusCode = Response.Status.NOT_FOUND;
            return Response.status(statusCode).build();
        }
    }

    @GET
    @Path("/location/list")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Fetch a list of building locations.")
    @ApiResponses({ @ApiResponse(code = 200, message = "List of buildings locations ready.", response = ListResponse.class),
            @ApiResponse(code = 500, message = "Failed to create a list.")})
    public Response getLocationList() {

        logger.info("getLocationList() called");
        Response.Status statusCode;

        try {
            ListResponse response = indoorPlanner.getLocationListResponse();
            return Response.ok(response, MediaType.APPLICATION_JSON_TYPE).build();
        } catch (Exception e){
            e.printStackTrace();
            statusCode = Response.Status.NOT_FOUND;
            return Response.status(statusCode).build();
        }
    }

}
