package cz.aic.indoorplanner;

import cz.aic.indoorplanner.processing.PlanBuilder;

public class Main {
    public static void main(String[] args) {
        PlanBuilder builder = new PlanBuilder();

        builder.createGraphFromPlan("src/main/resources/maps/skoda-mb/skoda-mb-v8_");
    }
}
