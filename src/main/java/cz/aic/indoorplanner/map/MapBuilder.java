package cz.aic.indoorplanner.map;

import cz.aic.indoorplanner.structures.map.Building;
import cz.aic.indoorplanner.structures.map.MapLevel;
import org.apache.log4j.Logger;
import cz.aic.indoorplanner.structures.geojson.*;
import cz.aic.indoorplanner.structures.mapbox.layout.LayoutFactory;
import cz.aic.indoorplanner.structures.mapbox.paint.PaintFactory;
import cz.aic.indoorplanner.structures.mapbox.Layer;
import cz.aic.indoorplanner.structures.mapbox.Source;
import cz.aic.indoorplanner.structures.navigation.NavigationNode;
import cz.aic.indoorplanner.structures.navigation.NavigationPath;

import java.util.*;

/**
 * Provides methods for manipulating with the map - layers, sources, levels, walls
 */
public class MapBuilder {
    private final static Logger logger = Logger.getLogger(MapBuilder.class);

    private Map<String, MapLevel> levels;                       /** a map linking levelId to level */
    private Map<String, Map<String, Source>> sources;           /** a map linking level to tuples (source_form, source) */
    private Map<Source, List<Layer>> layers;                    /** a map linking source to a list of layers */
    private Building building;
    private NavigationPath path;

    public MapBuilder() {
        levels = new HashMap<>();
        sources = new HashMap<>();
        layers = new HashMap<>();
        building = new Building();
    }

    /**
     * Adds a new level navigation graph to the map.
     *
     * @param level a level as specified in the JSON info file
     */
    public void addNavigationLevel(MapLevel level) {
        levels.put(level.getId(), level);

        if (!sources.containsKey(level.getGroupId())) { // create new layer and source in case of a new level group
            for (String form : Source.FORMS) {
                Source source = new Source(level.getGroupId(),
                        form,
                        "geojson"
                );
                sources.computeIfAbsent(level.getGroupId(), v -> new HashMap<>()).put(form, source);

                Layer layer = new Layer(source.getId(),
                        Layer.getFormStyle(form),       // layer style is based on the source style
                        source.getId(),
                        LayoutFactory.getLayout(form),
                        PaintFactory.getPaint(form)
                );
                layers.computeIfAbsent(source, v -> new ArrayList<>()).add(layer);
            }
        }
    }

    /**
     * Attaches a level geometry (building plan) to the specified level.
     *
     * @param levelId an id of a level for which the geometry should be added
     * @param featureCollection list of geojson features
     */
    public void addLevelGeometry(String levelId, FeatureCollection featureCollection) {
        for (Feature feature : featureCollection.getFeatures()) {
            addFeature(levels.get(levelId), "base", feature);

            if (feature.getGeometry().getType().equals("LineString")) {  // keep the list of walls for visibility analysis
                building.addWall(levelId, feature);
            }
        }
    }

    /**
     * Adds a new text label to the map.
     *
     * @param levelId an id of a level for which the label should be added
     * @param point a point with coordinates of the label
     * @param label a text of the label
     */
    public void addLabel(String levelId, Point point, String label) {
        Feature feature = new Feature(point);
        feature.addProperty("title", label);
        feature.addProperty("levelId", levelId);

        addFeature(levels.get(levelId), "labels", feature);
    }


    /**
     * Adds a new node to the map.
     *
     * @param node a node to be added
     * @param label a label to be shown next to the node
     * @param icon a node icon
     */
    public void addPathNode(NavigationNode node, String label, String icon) {
        Point point = new Point(node.getPosition());
        point.setName(label);

        Feature feature = new Feature(point);
        feature.addProperty("title", point.getName());
        feature.addProperty("icon", icon);

        addFeature(levels.get(node.getLevelId()), "path_points", feature);
    }

    /**
     * Adds a new path to the map.
     *
     * @param path a path to be added
     */
    public void addPath(NavigationPath path) {
        this.path = path;

        int i = path.getNodeCount() - 1;  // path is sorted in reversed order

        NavigationNode node = path.getNode(i);
        Polyline line = new Polyline();
        String levelId = node.getLevelId();

        // start the first edge
        line.addCoordinate(path.getNode(i).getPosition());

        for (i = path.getNodeCount() - 2; i >= 0; i--) {
            node = path.getNode(i);

            // level change
            if (! levels.get(node.getLevelId()).getGroupId().equals(levels.get(levelId).getGroupId())) {
                addPathNode(path.getNode(i+1), path.getEdge(i).getObject() + " to level " + levels.get(node.getLevelId()).getName(), "marker-15");
                addPathEdge(levelId, line);
                addPathNode(node, path.getEdge(i).getObject() + " from level " + levels.get(levelId).getName(), "marker-15");

                line = new Polyline();              // a new line needs to be created only once per level
                levelId = node.getLevelId();
            }
            line.addCoordinate(path.getNode(i).getPosition());
        }
        // add the last edge
        addPathEdge(levelId, line);
    }

    /**
     * Adds a new path edge to the map.
     *
     * @param levelId id of level to which the line should be added
     * @param line line to be added
     */
    private void addPathEdge(String levelId, Polyline line) {
        addFeature(levels.get(levelId), "path", new Feature(line));
    }

    /**
     * Adds a new feature to the source. Source is specified by a given level and form.
     *
     * @param level a level to which the feature should be added
     * @param form a form of a source to which the feature should be added
     * @param feature a feature to be added
     */
    private void addFeature(MapLevel level, String form, Feature feature) {
        try {
            Source source = sources.get(level.getGroupId()).get(form);
            List<Feature> featureList = source.getData().getFeatures();
            featureList.add(feature);
        } catch (NullPointerException e) {
            logger.info("Warning: level " + level.getName() + " not found.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns added sources of specified form(s)
     *
     * @param forms a filter, types of sources to return
     * @return a map of sources hashed by their id
     */
    public Map<String, FeatureCollection> getSources(String[] forms) {
        Map<String, FeatureCollection> map = new HashMap<>();

        for (Map.Entry<String, Map<String, Source>> levelEntry : sources.entrySet()) {
            for (Map.Entry<String, Source> sourceEntry : levelEntry.getValue().entrySet()) {
                String form = sourceEntry.getKey();
                Source source = sourceEntry.getValue();

                if (Arrays.asList(forms).contains(form)) {
                    map.put(source.getId(), source.getData());
                }
            }
        }
        return map;
    }


    /**
     * Returns added layers of specified form(s)
     *
     * @param forms a filter, types of layers to return
     * @return a list of layers
     */
    public List<Layer> getLayers(String[] forms) {
        List<Layer> list = new ArrayList<>();

        for (Map.Entry<String, Map<String, Source>> levelEntry : sources.entrySet()) {
            for (Map.Entry<String, Source> sourceEntry : levelEntry.getValue().entrySet()) {
                String form = sourceEntry.getKey();
                Source source = sourceEntry.getValue();

                if (Arrays.asList(forms).contains(form)) {
                    for (Layer layer : layers.get(source)) {
                        list.add(layer);
                    }
                }
            }
        }
        return list;
    }

    /**
     * Returns a sorted list of levels
     *
     * @return a list of levels sorted by their z-level
     */
    public List<MapLevel> getSortedLevels() {
        List<MapLevel> levelList = new ArrayList<>(levels.values());

        Collections.sort(levelList, (o1, o2) -> o1.getZ()-o2.getZ());
        return levelList;
    }

    /**
     * Returns a building - a representation of the wall geometry
     *
     * @return a building object
     */
    public Building getBuilding() {
        return building;
    }

    /**
     * Deletes sources of specified form(s)
     *
     * @param forms a filter, types of sources to delete
     */
    public void clearSources(String[] forms) {
        for (Map.Entry<String, Map<String, Source>> levelEntry : sources.entrySet()) {
            for (Map.Entry<String, Source> sourceEntry : levelEntry.getValue().entrySet()) {
                String form = sourceEntry.getKey();
                Source source = sourceEntry.getValue();

                if (Arrays.asList(forms).contains(form)) {
                    source.getData().getFeatures().clear();
                }
            }
        }
    }

    public NavigationPath getPath() {
        return path;
    }

    public MapLevel getLevelById(String id) {
        return levels.get(id);
    }
}
