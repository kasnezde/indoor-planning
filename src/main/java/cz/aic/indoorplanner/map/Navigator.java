package cz.aic.indoorplanner.map;

import com.fasterxml.jackson.core.type.TypeReference;
import cz.agents.basestructures.GPSLocation;
import cz.agents.basestructures.Graph;
import cz.aic.indoorplanner.IndoorPlanner;
import cz.aic.indoorplanner.algorithms.Dijkstra;
import cz.aic.indoorplanner.structures.analysis.Instruction;
import cz.aic.indoorplanner.structures.analysis.InstructionType;
import cz.aic.indoorplanner.structures.geojson.Feature;
import cz.aic.indoorplanner.structures.geojson.FeatureCollection;
import cz.aic.indoorplanner.structures.geojson.Point;
import cz.aic.indoorplanner.structures.geojson.Polyline;
import cz.aic.indoorplanner.structures.geometry.Coord;
import cz.aic.indoorplanner.structures.map.Building;
import cz.aic.indoorplanner.structures.navigation.NavigationEdge;
import cz.aic.indoorplanner.structures.navigation.NavigationGraph;
import cz.aic.indoorplanner.structures.navigation.NavigationNode;
import cz.aic.indoorplanner.structures.navigation.NavigationPath;
import cz.aic.indoorplanner.utils.IOUtils;
import cz.aic.indoorplanner.utils.JSONUtils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import static cz.agents.geotools.DistanceUtil.computeGreatCircleDistance;
import static cz.aic.indoorplanner.utils.GeometryUtils.pointsGPSDistance;

/**
 * Provides methods for manipulating with the navigation graph - nodes, edges, paths
 */
public class Navigator {
    private final static Logger logger = Logger.getLogger(IndoorPlanner.class);
    private NavigationGraph navigationGraph;
    private int navigationNodeId;                   /** a current id of a newly added node */
    private HashMap<String, Integer> nodeIdMap;     /** a mapping from unique ids to ids in the navigation graph */

    public Navigator() {
        navigationNodeId = 0;
        nodeIdMap = new HashMap<>();
        navigationGraph = new NavigationGraph();
    }

    /**
     * Find a path between two nodes in a graph
     *
     * @param fromNodeId an id of the start node
     * @param toNodeId an id of the end node
     * @return a path from the start node to the end node (null if path not found)
     */
    public NavigationPath findPath(int fromNodeId, int toNodeId, boolean accessibility) {
        Graph<NavigationNode, NavigationEdge> graph = navigationGraph.getGraph();

        NavigationNode startNode = navigationGraph.getNodeById(fromNodeId);
        NavigationNode targetNode = navigationGraph.getNodeById(toNodeId);

        Dijkstra dijkstra = new Dijkstra();
        NavigationPath path = dijkstra.findPath(graph, startNode, targetNode, accessibility);

        if (path == null) {
            logger.info("Path from " + fromNodeId + " to " + toNodeId + " not found." );
        }
        return path;
    }

    /**
     * Loads data for the navigation graph from a file
     *
     * @param filename a name of the input file
     */
    public void addNavigationFile(String filename) {
        String jsonStr = IOUtils.readTextFileFromResources(filename);

        FeatureCollection navigation = JSONUtils.convertJSONStringToDesiredObject(jsonStr,
                new TypeReference<FeatureCollection>() {
                });

        for (Feature feature : navigation.getFeatures()) {
            switch (feature.getGeometry().getType()) {
                // add points as nodes
                case "Point":
                    Point point = (Point) feature.getGeometry();

                    NavigationNode node = new NavigationNode(navigationNodeId, point.getCoordinates(), feature.getProperty("level"));
                    node.addProperty("placement", feature.getProperty("placement"));
                    node.addProperty("uniqueId", feature.getProperty("uniqueId"));
                    navigationGraph.addNavigationNode(node);

                    nodeIdMap.put(feature.getProperty("uniqueId"), navigationNodeId);
                    navigationNodeId++;
                    break;
                // add linestrings as edges
                case "LineString":
                    Polyline line = (Polyline) feature.getGeometry();

                    // start or end point of the line is missing, skip it
                    if (! (nodeIdMap.containsKey(feature.getProperty("from")) && nodeIdMap.containsKey(feature.getProperty("to")))) {
                        continue;
                    }
                    int fromId = nodeIdMap.get(feature.getProperty("from"));
                    int toId = nodeIdMap.get(feature.getProperty("to"));
                    double distance;
                    String object = feature.getProperty("object");

                    if (feature.hasProperty("distance") ) {
                        distance = Double.parseDouble(feature.getProperty("distance"));
                    } else {
                        Coord fromCoord = line.getCoordinates().get(0);
                        Coord toCoord = line.getCoordinates().get(1);

                        distance = computeGreatCircleDistance(fromCoord.getLat(), fromCoord.getLon(),
                                toCoord.getLat(), toCoord.getLon());
                    }
                    NavigationEdge edge = new NavigationEdge(fromId, toId, distance, object);
                    navigationGraph.addNavigationEdge(edge);
                    break;
                default:
                    logger.error("Unsupported geometry");
                    break;
            }
        }
    }

    /**
     * Creates a graph from loaded nodes and edges
     */
    public void createGraph() {
        navigationGraph.createGraph();
    }

    /**
     * Returns node with a given id
     *
     * @param id id of the node in the graph
     * @return a node with the id
     */
    public NavigationNode getNodeById(int id) {
        return navigationGraph.getNodeById(id);
    }

    /**
     * Returns the set of nodes in the graph
     *
     * @return a set of nodes in the graph
     */
    public Collection<NavigationNode> getNodes() {
        return navigationGraph.getNodes();
    }

    /**
     * Creates a reduced version of the path. In the new path, all the unnecessary nodes are skipped.
     * A node is unnecessary if the next node is visible from the previous node.
     *
     * @param building a building object containing a set of walls on the map
     * @param path a path to be reduced
     * @return a reduced version of the given path
     */
    public NavigationPath reducePath(Building building, NavigationPath path) {
        List<NavigationNode> nodes = path.getNodes();
        List<NavigationEdge> edges = path.getEdges();

        for (int i = 0; i < nodes.size()-3; i++) { // the nodes are taken in triples
            NavigationNode first = nodes.get(i);
            NavigationNode middle = nodes.get(i+1);
            NavigationNode last = nodes.get(i+2);

            if (first.getLevelId().equals(middle.getLevelId())
                    && middle.getLevelId().equals(last.getLevelId())
                    && first.getProperty("placement").equals("inside")
                    && middle.getProperty("placement").equals("inside")
                    && last.getProperty("placement").equals("inside")
                    && building.arePointsVisible(first.getLevelId(), first.getPosition(), last.getPosition())) {
                // remove the middle node if it is unnecessary
                nodes.remove(i+1);
                // change the first edge to join the first and the last node
                edges.set(i, new NavigationEdge(last.getId(), first.getId(), pointsGPSDistance(last.getPosition(), first.getPosition()), null));
                // remove the second edge
                edges.remove(i+1);
                i--;
            }
        }
        return path;
    }

    /**
     * Calculates a total length of the given edges
     *
     * @param path a path
     * @return a sum of the lengths of the edges in the path
     */
    public double calculatePathLength(NavigationPath path) {
        List<NavigationEdge> edges = path.getEdges();
        double distance = 0.0;

        for (NavigationEdge edge : edges) {
            distance += edge.getPreciseLength();
        }
        return distance;
    }


}
