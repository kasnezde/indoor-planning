# Indoor Planner

## Introduction
**Indoor Planner** is an application for creating navigation graphs and indoor planning.

The application consists of three components:

- The **plan builder** can be used to create a navigation graph from architectural drawing of a building.
- The **server** (backend) takes care of pathfinding on a navigation graph. 
- The **client** (frontend) is used by a user to interact with the server.

## Components
### Plan Builder
The plan builder can transform an architectural drawing to a navigation graph. The navigation graph can be used by other components for indoor navigation.

The architectural drawing of the building should be exported from a CAD system in the [**DXF format**](https://www.autodesk.com/techpubs/autocad/acad2000/dxf/dxf_format.htm) (other formats convertable to JSON are possible, but a small modification to the workflow is required)

Generating plan is mostly automatical, but user interaction is required in these cases:

- the plan have to be georeferenced
- a few coordinates have to be captured before the plan builder is used
- appropriate constants in the configuration file should be set for the current building
- in case some inconsistencies appear, the plan needs to be repaired manually
- levels have to be joined manually

For more information, see the instructions below.

#### Requirements
- Java 8
- QGIS (or a similar tool)
- ogr2ogr (or a similar tool)

#### Usage
 1. **Convert DXF to JSON**  
     
     Export architectonic drawing from ArchiCAD to DXF format. 

     Install [ogr2ogr](http://www.gdal.org/ogr2ogr.html) command line utility.  
     Use the following command to convert the file
     ```
    ogr2ogr -f GeoJSON <output-file>.json <input-file>.dxf
     ```
 2. **Georeference the JSON file**  
 
     Install the open source software [QGIS](http://www.qgis.org/en/site/).  

     Use Plugin manager to install the *Coordinate Capture* plugin (Získání souřadnic, *libcoordinatecaptureplugin.so*).

     Load the converted geojson of the building into QGIS. Use the plugin to capture **project coordinates** of several (at least 3) important points (e.g. corners of the building) and note them.

     In a new QGIS project use the *OpenLayers* plugin to add the OpenStreetMap layer.

     Locate the building on the map and get the **real world coordinates** of these points using the same approach.

     Use the following command to georeference the JSON file. After each `-gcp` follows the pair of corresponding points.

     ```
     ogr2ogr -f GeoJSON <output-file>.geojson -a_srs WGS84 -dsco ATTRIBUTES_SKIP=YES -order 1  -gcp <building-lat-1> <building-lon-1> <real-world-lat-1> <real-world-lon-1> -gcp <building-lat-2> (...) <input-file>.json
     ```
 3. **Capture coordinates of axes for a navigation grid**  
     For each level, use QGIS and *Coordinate Capture* to capture coordinates (latitude and longitude) of a top left corner, top right corner and bottom left corner.
      
     These points will serve as anchors of two axes denoting the rectangle shape surrounding the building. Inside the rectangle, a navigation grid will be created. The grid will be aligned with the axes.

     Save the coordinates for each level in the `map-info.json` file (see the map info file example).

 4. **Add GeoJSON as a new floor / building** 
  
     In the directory where the building plans are stored, create a new folder for location (in case it is a new location) or add the plan to the existing folder (in case it is a new building of an existing location).  

     Create / update a file `location-info.json` in the location folder accordingly.

     ```json
     {
        "id":"<map_id>",
        "location":{
            "type":"Point",
            "coordinates":[
                <location_longitude>,
                <location_latitude>
            ]
        },
        "drawing": {
                "levels":[
                    {
                        "id":"skoda-mb-0",
                        "group_id":"0",
                        "properties":{
                            "name":"0",
                            "type":"outdoor",
                            "zlevel":0,
                            "main":true
                        },
                        "obj_type":"Level"
                    }
                ],
                "properties":{
                    "is_root":true,
                    "display_name":"<drawing_display_name>",
                    "map_type":"exterior",
                    "name":"<drawing_name>"
                },
                "obj_type":"Drawing"
            },
        "buildings": [ 
            {
                "id": "<building_id>",
                "name": "<building_name>"
            },
        ],
        "properties":{
            "description":"<location_description>",
            "name":"<location_name>",
            "default_zoom": 17
        },
        "obj_type":"BuildingComplex"
     }
     ```
    
    In the location folder, create a new folder for building (in case it is a new building) or add the plan to the existing folder (in case it is a new level of an existing building).  

    Create / update a file `map-info.json` in the building folder accordingly.

    ```json
     {
        "id":"<map_id>",
        "drawing": {
            "levels":[
                {
                    "id":"<level_id>",
                    "group_id":"<level_group_id>",
                    "properties":{
                        "name":"<level_name>",
                        "type":"indoor",
                        "zlevel":0,
                        "main":false
                    },
                    "grid_axes": {
                        "top_left_x": <top_left_longitude>,
                        "top_left_y": <top_left_latitude>,
                        "top_right_x": <top_right_longitude>,
                        "top_right_y": <top_right_latitude>,
                        "bottom_left_x": <bottom_left_longitude>,
                        "bottom_left_y": <bottom_left_latitude>
                    },
                    
                    "obj_type":"Level"
                }
                ],
            "properties":{
                "is_root":true,
                "display_name":"<drawing_display_name>",
                "map_type":"building",
                "name":"<drawing_name>"
            },
            "obj_type":"Drawing"
        },
        "location":{
            "type":"Point",
            "coordinates":[
                <location_longitude>,
                <location_latitude>
            ]
        },
        "properties":{
            "description":"<building_description>",
            "name":"<building_name>"
        },
        "obj_type":"DXFMap"
    }
    ```

 5. **Create a navigation graph**  
      
     Use the PlanBuilder to create a navigation graph for all (!) levels in a folder.  

      ```java
      PlanBuilder builder = new PlanBuilder();
      builder.createGraphFromPlan("<path-to-the-folder-with-a-building-plan>");
      ```

#### Navigation between buildings
Separate buildings can be connected by paths within a location. The nodes and edges for the navigation graph are defined in the file `location.nav.json` located in the directory of the location. 

The file contains a GeoJSON FeatureCollection where geometric entities `Point` correspond to nodes and `LineString` to edges (the same way as inside of the buildings). The points have to have a property `uniqueId` set to an unique id and an attribute `placement` set to `outside`. The edges can join either outside points (defined in this file) or existing points in the buildings. Joined points are defined by their unique id in the attributes `from` and `to`.

The file can be created manually or using QGIS (see the Manual editing section).

#### Navigation between levels
Separate floors can be connected within a building. The edges for the navigation graph are defined in the file `map.nav.json` located in the directory of the building. 

The file contains a GeoJSON FeatureCollection where geometric entities `LineString` correspond to edges. The edges should join existing points in the building, where each of the point is on a separate floor. Joined points are defined by their unique id in the attributes `from` and `to`.

#### Manual editing
As the building plans and navigation files comply with the GeoJSON standard, their manual editing can be done using the QGIS software. Keep in mind that all the points have to have a property `uniqueId` with an unique id. 

Besides tools provided by QGIS, several Python scripts in the `src/main/resources/dxf/pytools` directory can be used for making the job easier:

- `join.py` - joins every point with every point by an edge within a selected group of points
- `point_tool.py` - tool for inserting new points (with a new unique id)
- `unique_id.py` - prints next unique id which can be used for creating a new point

The scripts work with default layer names which can be edited in the scripts.

### Backend  
The backend uses map plans and navigation files to respond to user requests. It takes care of indoor path planning.

#### Requirements
- Java 8
- Maven
- Jetty server

#### Usage
Type 

```
mvn clean jetty::run
```

in the project root directory.

### Frontend

#### About
The frontend can be used by a user. It displays map plans in a browser using Mapbox API. User can interact with the frontend to navigate in the location.

#### Requirements
- an internet browser with Javascript enabled

#### Usage
Open the file `index.html` in a browser. The file is located by default in the directory `src/main/resources/html/`.
  
If a server is running, but the frontend is not working, check if the variable *APP_SERVER* in the file `src/main/resources/js/variables.js` is set properly.

## Configuration
The application should be properly configured at first. The configuration with default values is in the file `config.properties` located in `src/main/resources`. The parameters are explained in detail in the configuration file.